#######################################################
# Unnamed isometric game, 2006 The Mad Cacti
# This file contains core engine components. Map and plot specific stuff is in iso-maps.hss

include, plotscr.hsd
include, scancode.hsi
include, lib2.hss      #swap for 3rd party hsi?
include, iso-maps.hss  #map specific scripts
include, itemsys.hss   #item menu
include, iso.hsi

#######################################################
###################  DECLARATIONS  ####################

define script, begin
  3, newgame, 0

  1, main, none
  -1, do player checks, 0
  -1, enter map, 0
  -1, create overhead, 0
  -1, update overhead, 0
 
  -1, basic door, 0
  -1, wall blocks, 3, 0, 0, 0  #x, y (in tiles), direction
  -1, walk, 2, 0, 0  #object, dir

  -1, use object, 0

  -1, put piece, 1, 0

  -1, create object, 4, 0, 0, 0, 8 #id  (npc type), x, y, offset in pixels from base of tile (must be 0 - 9)
  -1, build object, 2, 0, 8  #npc, foot offset (0-9)

  #-1, correct refs, 1, 0 # offset of OBJECT object
  -1, sort sprites, 3, 0, 0, 0  # offset of OBJECT object, x,y of square moving to

  -1, object at, 3, 0, 0, 0 #x, y, ignore    x and y in tiles, ignore is npc ref to ignore
  -1, sprite at, 3, 0, 0, 0

  -1, find tile, 3, 0, 0, 0 #x, y, dir,   passes tile back in passback x/y


  2, key handle, 0

  ## battle system
  -1, swing weapon, 2, 100, 0  #object, dir
  -1, damage, 2, 0, 1   #who, how much

  ## object triggered scripts
  4, destroy forever, 2, 0, 0
  5, mutate, 2, 0, 0

  ## ai scripts
  -1, ai:chase, 1, 0  #(object) movetype:chase

  -1, setup ai, 1, 0   #object

  ## misc
  -1, negativity, 1, 0
  -1, abs, 1, 0

end

#######################################################

define operator, begin
  60 && and
  60 || or
end

#######################################################

global variable, begin
  1, player x     #multiple of 20
  2, player y     #multiple of 10
  3, player tx    #tile x
  4, player ty
  5, player dir
  6, player frame
  7, sign x   #temp vars!
  8, sign y

 
  10, player npc
  11, parity
  12, player object  #global offset
  13, player want move  #direction, -1 for none
  14, player want use
  15, player hp

  #npc globals
  20, attack npc  #npc ref used for attack animations

  40, enemy turn

  50, game state
  51, input state   # whether the player can enter input
  53, room ticks
  54, use delay
  55, map trigger callback
  56, room number
  57, eachstep callback
  58, eachtick callback

  #item sys globals
  80, num items   # on menu

  #misc globals
  96, tb1
  97, tb2
  98, passback x
  99, passback y

  #100 - 300 is for objects
  #500+ is for item menu
end

#######################################################

define constant, begin
  0, Dir up  #directions are 0-8 clockwise.

  0, state: wait
  1, state: move
  2, state: plot    #textbox displayed, doors etc

  # Objects array for storing everything interactive or 
  100, OBJECTS_OFFSET  #global id offset
  49, OBJECTS_MAX      #num elements - 1
  200, OBJECTS_SIZE    #total size of array
  4, OBJECTS_SIZEOF    #size of 1 element
  0, OBJECTS:NPC
  1, OBJECTS:NPC2
  2, OBJECTS:SCRIPT    #ID of script torun to take actions
  3, OBJECTS:HP

end


#######################################################
##################  BEGIN SCRIPTS  ####################


script, new game, begin
  show backdrop (0)

  wait (20)

  fade screen out
  wait
  #teleport to map (1, 0, 0)
  show map
  wait

  load room (2, 18, 26)
  main  

end

#######################################################

script, main, begin
  suspend player 

  player dir := right

  player hp := 5
  create overhead

  game state := state: wait
  input state := true
  player want use := false
  player want move := -1

  #wait
  #fade screen in
  #wait

  while (1) do, begin
    variable (temp, i, last time, p gx, p gy)
 
    ## keep variables up to date
    do player checks
    if (last time <> seconds of play) then (enemy turn := true)
    last time := seconds of play
    

    ## player input
    if (game state == state: wait) then (
      if (player want move <> -1) then (
        game state := state: move

        find tile (player tx, player ty, player want move)
        if (wall blocks (player tx, player ty, player want move)) else (walk (player object, player want move))

        player dir := player want move
        player want move := -1
        game state := state: wait
      ) else (
        if (player want use) then (
          if (use delay) then (
            use delay -= 1
          ) else (
            #find tile (player tx, player ty, player dir)
            #temp := object at (passback x, passback y, read global (player object + OBJECTS:NPC2))
            #if (temp) then (use object (temp))
            use object
          )
        ) else (use delay := 0)
      )
    )

    ## enemy/scripted actions
    if (enemy turn) then (
      for (i, OBJECTS_OFFSET, OBJECTS_OFFSET + OBJECTS_SIZE -- 1, OBJECTS_SIZEOF) do (
        if (read global (i + OBJECTS:SCRIPT) <> 0 && random (0, 1) == 0) then (
          run script by id (read global (i + OBJECTS:SCRIPT), i)
        )
      )
      enemy turn := false
    )

    ## debug code
   # clear string (0)
   # append number (0, npc x (player npc))
   # append ascii (0, 32)
   # append number (0, npc y (player npc))
   # append ascii (0, 32)
   # append number (0, player object)
   # append ascii (0, 32)
   # append number (0, attack npc)
   # append ascii (0, 32)
   # append number (0, read global (player object + OBJECTS:NPC2))
    #append ascii (0, 32)
    #append number (0, pillar)
    #0 $+ 2

  #p gx := player ty + (player tx + 1) / 2 + (player tx + 1), mod, 2
  #p gy := player ty -- (player tx + 1) / 2


  #append ascii(0, 32)
  #append number (0, p gx)
  #append ascii(0, 32)
  #append number (0, p gy)


   # show string (0)

    #show value (npc pixel x (player npc) * 1000 + npc pixel y (player npc) + keyispressed(key:numlock) * 10000)

    
    room ticks += 1
    if (each tick callback) then (runscriptbyid (each tick callback))
    wait
  end
end

#######################################################

script, do player checks, begin
  player npc := read global (player object + OBJECTS:NPC)
  camera follows npc (player npc)
  player tx := npc pixel x (player npc) / 20
  player ty := npc pixel y (player npc) / 20

    #player tx := (player x -- 20) / 20  #co ordinates of the tile to the upper left of the player
    #player ty := (player y -- 10) / 20
    #player tx := player x / 20  #co ordinates of the tile to the upper left of the player
    #player ty := player y / 20
end

#######################################################

script, enter map, begin
  variable (i, temp, offset, hp)

  # clear the objects array
  for (i, OBJECTS_OFFSET, OBJECTS_OFFSET + OBJECTS_SIZE -- 1, 1) do (write global (i, 0))

  # create special npcs
  attack npc := create npc (35, 0, 0, 0)

  # create special objects

  player object := create object (0, player tx, player ty)
  player npc := read global (player object + OBJECTS:NPC)

  player x := npc pixel x (player npc)
  player y := npc pixel y (player npc)

  camera follows npc (player npc)

  set footoffset (-9)
  set on keypress script (@key handle)

  # now build the objects array
  for (i, 1, 35) do (
    temp := npc reference (i)
    if (temp) then (
      switch (get npc id (temp)) do (
        case () do (
        )
        else (offset := 8, hp := 9999)
      )
      build object (temp, offset)
    )
  )

  room ticks := 0

end

#######################################################

script, create overhead, begin
  variable (i)
  for (i, 1, player hp) do (
    append ascii (27, 133)
    append ascii (28, 133)
    append ascii (29, 133)
    append ascii (30, 133)
    append ascii (31, 133)
  )

  string color (27, 0)
  string color (28, 0)
  string color (29, 0)
  string color (30, 0)
  string color (31, 88)
  string style (27, string:flat)
  string style (28, string:flat)
  string style (29, string:flat)
  string style (30, string:flat)
  string style (31, string:flat)

  show string at (27, 11, 10)
  show string at (28, 9, 10)
  show string at (29, 10, 9)
  show string at (30, 10, 11)
  show string at (31, 10, 10)

end

#######################################################

script, update overhead, begin
  clear string (31)

  variable (i)
  for (i, 1, player hp) do (
    append ascii (31, 133)
  )
end

#######################################################

#unused
script, basic door, begin
  fade screen out
  wait
  # goto
  fade screen in
  wait
end

#######################################################

script, key handle, begin
  #variable (d)
  #sign x := 0
  #sign y := 0

  if (key is pressed (key: esc)) then (game over)
  if (key is pressed (key: p)) then (parity := parity, xor, 1, show value (99))

  if (key is pressed (key:tilde)) then (
    $9="map state dump "
        append number (9, player object)
    $10="at: "
    trace (9)
    variable (i, j)
    for (i, npc x (player npc), npc x (player npc) + 8) do (
      for (j, npc y (player npc), npc y (player npc) + 5) do (
        9 $= 10
        append number (9, i)
        append ascii (9, 32)
        append number (9, j)

        append ascii (9, 32)
        append number (9, object at(i,j))
        trace(9)
      )
    )
  )

  if (input state) then (
 
    player want move := -1
    if (key is pressed (key: left)) then (player want move := 7)
    if (key is pressed (key: right)) then (player want move := 3)
    if (key is pressed (key: up)) then (player want move := 1)
    if (key is pressed (key: down)) then (player want move := 5)
  
    if (key is pressed (key: space) || key is pressed (key: ctrl) || key is pressed (key: enter)) 
    then (player want use := true)

    #inventory
    if (key is pressed (key: i)) then (showvalue (item screen))
  )
end

#######################################################

#takes info from passback x/y
script, wall blocks, temp x, temp y, dir, begin

  #$2 ="blocked by "
  #append number (2,passback x)
  #append ascii (2,32)
  #append number (2,passback y)
  #append ascii (2,32)
  #then ($2+"harm" exit returning (true))
  if (read pass block (passback x, passback y) && harm tile) then (exit returning (true))

  #$2+"wall"

  #if (y pos, mod, 20) then (  #if player stands between 2 tiles
  if ((temp x, and, 1) xor, parity) then (
    switch (dir) do (
      case (1) do (  # up-right
        if (read pass block (temp x + 1, temp y), and, north wall) then (exit returning (true)) else (sign x := 1, sign y := -1) 
      )
      case (3) do (  # down-right
        if (read pass block (temp x + 1, temp y), and, south wall) then (exit returning (true)) else (sign x := 1, sign y := 1)
      )
      case (5) do (  # down-left
        if (read pass block (temp x, temp y), and, south wall) then (exit returning (true)) else (sign x := -1, sign y := 1)
      )
      case (7) do (  # up-left
        if (read pass block (temp x, temp y), and, north wall) then (exit returning (true)) else (sign x := -1, sign y := -1)
      )
    )
  ) else (  #if player stands between 4 tiles
    switch (dir) do (
      case (1) do (  # up-right
        if (read pass block (temp x + 1, temp y), and, south wall) then (exit returning (true)) else (sign x := 1, sign y := -1)
      )
      case (3) do (  # down-right
        if (read pass block (temp x + 1, temp y + 1), and, north wall) then (exit returning (true)) else (sign x := 1, sign y := 1)
      )
      case (5) do (  # down-left
        if (read pass block (temp x, temp y + 1), and, north wall) then (exit returning (true)) else (sign x := -1, sign y := 1)
      )
      case (7) do (  # up-left
        if (read pass block (temp x, temp y), and, south wall) then (exit returning (true)) else (sign x := -1, sign y := -1)
      )
    )
  )

  #clear string(2)

end

#######################################################

#not in use
script, putpiece, d, begin
  player dir := d
  player x += sign x * 20
  player y += sign y * 10
  put npc (player npc, player x -- 10, player y -- 10)
end

#######################################################

# takes arguments from signx and signy and passback x/y

# passmap doc:
# harmtile means object standing (blocking) on tile
# vechicleA means standon trigger (including doors)

script, walk, object, dir, begin
  variable (ctr, x, y, npc, npc2, tile stuff)

  input state := false



  #npc := read global (object + OBJECTS:NPC)

  #find tile (npc x (npc), npc y (npc), dir)


  npc := sort sprites (object, passback x, passback y)

  do player checks
 
  npc2 := read global (object + OBJECTS:NPC2)

  x := npc pixel x (npc)
  y := npc pixel y (npc)

  #npc2 := npc extra (npc, extra 2)

  tile stuff := read pass block (x / 20, y / 20)
  write pass block (x / 20, y / 20, tile stuff && (255 -- harm tile))
  write pass block (passback x, passback y, read pass block (passback x, passback y) || (tile stuff && harm tile))
 

  for (ctr, 1, 5) do (
    x += sign x * 4
    y += sign y * 2

    put npc (npc, x, y)
    if (npc2) then (put npc (npc2, x, y -- 20))

    if (eachstep callback) then (run script by id (eachstep callback, dir))

    if (ctr == 5) then (input state := true)
    else (wait, room ticks += 1)   #glup!
  )

  if (map trigger callback) then (
    if (read pass block (passback x, passback y) && vehicle A) then (
    #  $2=" trigger "
    #  append number (2, map trigger callback)
      input state := false
      game state := state: plot
      run script by id (map trigger callback, passback x, passback y, object)
      input state := true
    )
  )
end

#######################################################

# script for the player to "use" an object. includes attacking
script, use object, begin
  variable (object, attack)

  find tile (player tx, player ty, player dir)
  object := object at (passback x, passback y, read global (player object + OBJECTS:NPC2))
  if (object) then (
     #deicde whether enemy
      attack := true
  )

  if (attack || object == 0) then (
    # swing sword
    swing weapon (player object, player dir)
  ) else (
    # interact
    use npc (object)
    game state := state: plot
    wait
    wait for textbox
    player want use := false
    game state := state: wait
    use delay := 7
  )

     

end

#######################################################

definescript(-1,createobjectold,3,0,0,0)
script, create object old, id, x, y, begin
end

#######################################################

#place something new on the map. returns object offset
#offset is pixels above base of tile. Must be 0 - 9
script, create object, id, x, y, offset, begin
  variable (i, npc1, npc2)
  for (i, OBJECTS_OFFSET, OBJECTS_OFFSET + OBJECTS_SIZE -- 1, OBJECTS_SIZEOF) do (
    if (read global (i + OBJECTS:NPC)) else (

      write pass block (x, y, read pass block (x, y) || harm tile)

      # foot offset is set to up 9 pixels
      if (x, and, 1) then (
        x := x * 20 + 10
        y := y * 20 + 9 -- offset
      ) else (
        x := x * 20 + 10
        y := y * 20 + 19 -- offset
      )
    
      npc1 := create npc (id, 0, 0, 0)
      #set npc frame (npc1, 0)
      put npc (npc1, x, y)
    
      npc2 := create npc (id, 0, 0, 1)
      #set npc frame (npc2, 1)
      put npc (npc2, x, y -- 20)
    
      #set npc extra (npc1, extra 2, npc2)
      #set npc extra (npc2, extra 2, npc1)
    
      write global (i + OBJECTS:NPC, npc1)
      write global (i + OBJECTS:NPC2, npc2)
      set npc extra (npc1, extra 1, i)
      set npc extra (npc2, extra 1, i)
      #set npc extra (npc2, extra 1, 0)


      if (id <> 0) then (setup ai (i))  # don't do for player
    
      exit returning (i)

    )
  )

  show textbox (2) #too many objects!

end

#######################################################

#turn a placemarker npc into an object
#place something new on the map. returns object offset
#offset is pixels above base of tile. Must be 0 - 9
script, build object, npc, offset, begin
  variable (i, x, y, npc2)
  for (i, OBJECTS_OFFSET, OBJECTS_OFFSET + OBJECTS_SIZE -- 1, OBJECTS_SIZEOF) do (
    if (read global (i + OBJECTS:NPC)) else (
      x := npc x (npc)
      y := npc y (npc)

      write pass block (x, y, read pass block (x, y) || harm tile)

      # foot offset is set to up 9 pixels
      if (x, and, 1) then (
        x := x * 20 + 10
        y := y * 20 + 9 -- offset
      ) else (
        x := x * 20 + 10
        y := y * 20 + 19 -- offset
      )
    
      set npc direction (npc, 0)
      put npc (npc, x, y)
    
      npc2 := create npc (get npc id (npc), 0, 0, 1)
      put npc (npc2, x, y -- 20)
    
      write global (i + OBJECTS:NPC, npc)
      write global (i + OBJECTS:NPC2, npc2)
      set npc extra (npc, extra 1, i)
      set npc extra (npc2, extra 1, i)
      #set npc extra (npc2, extra 1, 0)

      setup ai (i)
    
      exit returning (i)

    )
  )

  show textbox (2) #too many objects!

end


#######################################################


# returns npc
script, sort sprites, object, new x, new y, begin
  variable (npc ref1, npc ref2, npc ref3, new ref, temp)

  # correct lower npc
  
  npc ref1 := read global (object + OBJECTS:NPC)  #must be on bottom (greatest ref)
  #OMG DELICATE
  npc ref2 := sprite at (npc x (npc ref1), npc y (npc ref1), npc ref 1)
  npc ref3 := sprite at (new x, new y, npc ref 1)

  if (npc ref2 && npc ref1 << npc ref2) then (new ref := npc ref2)
  if (npc ref3 && npc ref1 << npc ref3) then (new ref := npc ref3)

  if (new ref) then (
    #swap
    swap npc ref (npc ref1, new ref)

    #correct datas
    write global (object + OBJECTS:NPC, new ref)
    write global (npc extra (new ref, extra 1) + OBJECTS:NPC, npc ref1)

    #correct npc extra info
    temp := npc extra (npc ref1, extra 1)
    set npc extra (npc ref1, extra 1, npc extra (new ref, extra 1))
    set npc extra (new ref, extra 1, temp)
    #set npc extra (npc ref1, extra 1, new ref)
    #set npc extra (new ref, extra 1, npc ref1)

    #write global (npc extra (new ref, extra 1) + OBJECTS:NPC, npc ref1)
    #set npc extra (npc extra (new ref, extra 2), extra 2, npc ref1)

    return (new ref)
  ) else (return (npc ref1))


  # do the same for upper npc!!
  
  npc ref1 := read global (object + OBJECTS:NPC2)  #must be on top (least ref)
  npc ref2 := sprite at (npc x (npc ref1), npc y (npc ref1), npc ref 1)
  npc ref3 := sprite at (new x, new y -- 1, npc ref 1)

  new ref := false
  if (npc ref1 >> npc ref2) then (new ref := npc ref2)
  if (npc ref1 >> npc ref3) then (new ref := npc ref3)

  if (new ref) then (
    #swap
    swap npc ref (npc ref1, new ref)

    #correct datas
    write global (object + OBJECTS:NPC2, new ref)
    write global (npc extra (new ref, extra 1) + OBJECTS:NPC2, npc ref1)

    #correct npc extra info
    temp := npc extra (npc ref1, extra 1)
    set npc extra (npc ref1, extra 1, npc extra (new ref, extra 1))
    set npc extra (new ref, extra 1, temp)
  )
end

#######################################################

# x and y in tiles, ignore is npc ref to ignore
# return object id: ignores npcs which are not part of an object, only finds from bottom npc
script, object at, x, y, ignore, begin
  variable (i, temp, temp2)
  #x := x * 20 + 15
  #y := y * 20 + 15
  for (i, 0, npc at spot (x, y, get count) -- 1) do (
    #temp := npc at spot (x, y, i)
    #temp2 := npc extra (temp, extra 1)
    #if (temp <> ignore && temp2 <> 0) then (exit returning (temp2))
    temp2 := npc extra (npc at spot (x, y, i), extra 1)

 # $19="check obj: "
 # appendnumber(19, i)
 # appendascii(19,32)
 # appendnumber(19, temp2)
 # trace (19)

    if (temp2 <> 0) then (exit returning (temp2))
  )
end

#######################################################

# x and y in tiles, ignore is npc ref to ignore
# return npc ref: ignores npcs which are not part of an object, finds any pices of an object
script, sprite at, x, y, ignore, begin
  variable (i, temp)
  #x := x * 20 + 15
  #y := y * 20 + 15
  for (i, 0, npc at spot (x, y, get count) -- 1) do (
    temp := npc at spot (x, y, i)
    if (temp <> ignore && npc extra (temp, extra 1) <> 0) then (exit returning (temp))
  )
end

#######################################################

script, find tile, x, y, dir, begin

  #if (y pos, mod, 20) then (  #if player stands between 2 tiles
  if ((x, and, 1) xor, parity) then (
    switch (dir) do (
      case (0) do (  # up
        passback x := x
        passback y := y -- 1
      )
      case (1) do (  # up-right
        passback x := x + 1
        passback y := y -- 1
      )
      case (2) do (  # right
        passback x := x + 2
        passback y := y
      )
      case (3) do (  # down-right
        passback x := x + 1
        passback y := y
      )
      case (4) do (  # down
        passback x := x
        passback y := y + 1
      )
      case (5) do (  # down-left
        passback x := x -- 1
        passback y := y
      )
      case (6) do (  # left
        passback x := x -- 2
        passback y := y
      )
      case (7) do (  # up-left
        passback x := x -- 1
        passback y := y -- 1
      )
    )
  ) else (  #if player stands between 4 tiles
    switch (dir) do (
      case (0) do (  # up
        passback x := x
        passback y := y -- 1
      )
      case (1) do (  # up-right
        passback x := x + 1
        passback y := y
      )
      case (2) do (  # right
        passback x := x + 2
        passback y := y
      )
      case (3) do (  # down-right
        passback x := x + 1
        passback y := y + 1
      )
      case (4) do (  # down
        passback x := x
        passback y := y + 1
      )
      case (5) do (  # down-left
        passback x := x -- 1
        passback y := y + 1
      )
      case (6) do (  # left
        passback x := x -- 2
        passback y := y
      )
      case (7) do (  # up-left
        passback x := x -- 1
        passback y := y
      )
    )
  )

end

#######################################################

#animation only
script, swing weapon, object, dir, begin
  variable (npc, i, frames, frame)
 
  npc := read global (object + OBJECTS:NPC)

  # look at the enemy graphic to determine which weapon graphic to use
  switch (read npc (npc, NPCstat:picture)) do (
    case (0, 7) do (  # giant sword (hero, skeleton)
      alter npc (npc, NPCstat:picture, 8 + dir / 4)  # sword
      alter npc (npc, NPCstat:palette, 2)  # sword
      frames := 4
    )
  )

  switch (dir) do (
    case (1) do ( # up-right
      put npc (attack npc, npc pixel x (npc) + 20, npc pixel y (npc) -- 30)
    )
    case (3) do ( # down-right
      put npc (attack npc, npc pixel x (npc) + 20, npc pixel y (npc) + 10)
    )
    case (5) do ( # down-left
      put npc (attack npc, npc pixel x (npc) -- 20, npc pixel y (npc) + 10)
    )
    case (7) do ( # up-left
      put npc (attack npc, npc pixel x (npc) -- 20, npc pixel y (npc) -- 30)
    )
  )

  # animate
  frame := frames * ((dir / 2), mod, 1)

  for (i, frame, frame + frames -- 1) do (
    set npc direction (attack npc, i / 2)
    set npc frame (attack npc, i, mod, 2)
    wait (2)
  )

  put npc (attack npc, 0, 0)

  find tile (npc x (npc), npc y (npc), dir)

  damage (object at (passback x, passback y), 1)
end

#######################################################

script, damage, object, amount, begin

  if (object == player object) then (
    player hp -= amount
    update overhead
    if (player hp <= 0) then (
      wait (3)
      fade screen out (63, 0, 0)
      wait
      fade screen out (0, 0, 0)
      wait
      show textbox (7)
      wait (30)
      gameover
    )
  ) else (
    variable (temp)
    temp := read global (object + OBJECTS:HP)
    temp -= amount
    write global (object + OBJECTS:HP, temp)
    if (temp <= 0) then (
      use npc (read global (object + OBJECTS:NPC))
    )

  )
      

end



#######################################################
### The following are (non-plot) scripts run from npcs when activated, to die etc

# turn on a tag to signal this npc is ded
script, destroy forever, tag, npc, begin
  variable (obj)
  obj := npc extra (npc, extra 1)

  # maybe false the palette, do some effect here

  set tag (tag, on)

  #destroy npc (read global (obj + OBJECTS:NPC2))
  write global (obj + OBJECTS:NPC, 0)
  write global (obj + OBJECTS:NPC2, 0)
end

#######################################################

# toggle a tag to kill this npc and create a new one in its place,  eg. chests
# we assume that the new npc has the next id from the current one if setting the tag
# or the previous id if unsetting
script, mutate, tag, npc, begin
  variable (obj, id, x, y)
  obj := npc extra (npc, extra 1)
  id := get npc id (npc)
  if (check tag (tag)) then (id -= 1) else (id += 1)
  x := npc pixel x (npc)
  y := npc pixel y (npc)

  # maybe support animations here

  wait for textbox
  set tag (tag, check tag (tag) xor, 1)
  # the npc is destroyed

  npc := npc reference (id, 0)
  set npc extra (npc, extra 1, obj)
  set npc direction (npc, 0)
  put npc (npc, x, y)
  write global (obj + OBJECTS:NPC, npc)

  npc := create npc (id, 0, 0, 1)
  set npc extra (npc, extra 1, obj)
  put npc (npc, x, y -- 20)
  write global (obj + OBJECTS:NPC2, npc)
end



#######################################################
### The following are AI scripts for objects

script, ai:chase, object, begin
  variable (ox, oy, o gx, o gy, p gx, p gy, x diff, y diff, dir)

  ox := read global (object + OBJECTS:NPC)
  oy := npc y (ox)
  ox := npc x (ox)

  # these are coordinates on the isometric GRID of the object and the player

  o gx := oy + (ox + 1) / 2 + (ox + 1), mod, 2
  o gy := oy -- (ox + 1) / 2

  p gx := player ty + (player tx + 1) / 2 + (player tx + 1), mod, 2
  p gy := player ty -- (player tx + 1) / 2

  clear string (2)
  append ascii(2, 32)
  append number (2, o gx)
  append ascii(2, 32)
  append number (2, o gy)

  # now figure out which direction to walk in

  x diff := abs (p gx -- o gx)
  y diff := abs (p gy -- o gy)

  if (x diff + y diff == 1) then (
    #attack!
#exitscript
    if (p gx -- o gx == 1) then (swing weapon (object, 3))
    if (p gx -- o gx == -1) then (swing weapon (object, 7))
    if (p gy -- o gy == 1) then (swing weapon (object, 5))
    if (p gy -- o gy == -1) then (swing weapon (object, 1))

  ) else (

    dir := random (0, 3) * 2 + 1

    #if (p gy >> o gy) then (dir := 1)
    #if (p gy << o gy) then (dir := 5)

    #if (p gx >> o gx) then (dir := 3)
    #if (p gx << o gx) then (dir := 7)

    #if (((x diff, xor, y diff) << 0) xor, (x diff << y diff)) then (
    if (x diff <= y diff) then (
      if (p gy >> o gy) then (dir := 5)
      if (p gy << o gy) then (dir := 1)
    ) else (
      if (p gx >> o gx) then (dir := 3)
      if (p gx << o gx) then (dir := 7)
    )

    find tile (ox, oy, dir)
    if (wall blocks (ox, oy, dir)) then (
      # there's something in the way, try another direction
      if (x diff >> y diff) then (
        if (p gy >> o gy) then (dir := 5)
        if (p gy << o gy) then (dir := 1)
       ) else (
        if (p gx >> o gx) then (dir := 3)
        if (p gx << o gx) then (dir := 7)
      )

      find tile (ox, oy, dir)
      if (wall blocks (ox, oy, dir)) else (walk (object, dir))
    ) else (walk (object, dir))
  )

  append ascii(2, 32)
  append number (2, dir)

end

#######################################################

script, setup ai, object, begin
  variable (npc)

  npc := read global (object + OBJECTS:NPC)
  switch (read npc (npc, NPCstat:movetype)) do (
    case (NPCmovetype:standstill) do (
      write global (object + OBJECTS:SCRIPT, 0)
    )
    case (NPCmovetype:chaseyou) do (
      write global (object + OBJECTS:SCRIPT, @ai:chase)
    )
    else (write global (object + OBJECTS:SCRIPT, 0))
  )
 
end



#######################################################
### Misc scripts

script, negativity, a, begin
  if (a << 0) then (return (-1)) else (return (1))
end

#######################################################

script, abs, a, begin
  if (a << 0) then (return (a * -1)) else (return (a))
end