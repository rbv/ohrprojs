include, plotscr.hsd
#include, buggy.hsi
include, scancode.hsi
include, animation scripts.txt

define script (1,NPC Autorunner,none)
define script (2,Hero Mover,none)
define script (3,Hero Counter,none)

#Note: The first two scripts are for an old test and do not relate
#      to the animation scripts.

script,NPC Autorunner,begin
variable (i)
for (i,1,5000) do(
walk npc (0,north,2)
wait for npc (0)
walk npc (0,west,2)
wait for npc (0)
walk npc (0,south,2)
wait for npc (0)
walk npc (0,east,2)
wait for npc (0)
)
end

script,Hero Mover,begin
suspend player
show text box (1)
wait for text box
walk hero (0,south,4)
wait for hero (0)
resume player
end

#Animation scripts begin here.

script,Hero Counter,begin
suspend player
suspend obstruction
show text box (3)
wait for text box
hero animation (0,3,0,8,5,false,0)

#counts to 8 at 2 ticks per animation

show text box (4)
wait for text box
hero animation (0,3,0,16,2)

#counts to 16 at 2 ticks per animation
#Note: This uses two consecutive walkabout sets automatically

show text box (5)
wait for text box
hero animation (0,3,0,24,2)

#counts to 24 at 2 ticks per animation
#Note: This uses three consecutive walkabout sets automatically

show text box (6)
wait for text box
hero animation (0,3,0,12,2,false,0,false,12,@hero walk 1)

#counts to 12, then runs the first walking script

show text box (8)
wait for text box
hero animation (0,3,0,24,2,false,0,false,14,@hero walk 2)

#counts to 14, breaks, runs second walking script, continues to 24

show text box (7)
wait for text box
npc animation (1,3,0,10,5,false,0,false,10,@npc walk)

#npc counts to 10 at 5 ticks per animation, then walks away

show text box (9)
wait for text box
hero animation (0,3,0,18,2,true)
wait (20)

#counts to 18 then freezes for a second

show text box (10)
wait for text box
hero animation (0,3,0,18,4,true,4,true)

#counts backward from 18 to 4 at 4 ticks per animation then freezes frame

show text box (11)
wait for text box
hero animation (0,3,0,19,4,true,4)
wait (20)

#counts from 4 to 19 and freezes for one second

hero animation (0,3,0,19,4,true,13,true)
wait (20)

#counts from 19 to 13 and freezes for a second

hero animation (0,3,0,24,4,false,13,false,24,@hero walk 1)
set hero picture (0,0)

#counts from 13 to 24, walks away, then reverts to its normal form

resume obstruction
resume player
end

script,Hero Walk 1,begin
set hero picture (0,0)
walk hero (0,west,3)
wait for hero (0)
end

script,Hero Walk 2,begin
set hero picture (0,0)
walk hero (0,east,3)
wait for hero (0)
end

script,NPC Walk,begin
alter npc (1,npcstat:picture,1)
walk npc (1,south,3)
wait for npc (1)
end
