
  # Create some square images for the cube
  convert ../terrain.png -crop 16x16+0+16 +repage  top.png
  convert ../terrain.png -crop 16x16+0+16 +repage  left.png
  convert ../terrain.png -crop 16x16+0+16 +repage  right.png


  # top image shear.
  convert top.png -interpolate nearest -resize  260x301! -alpha set -background none \
          -shear 0x30 -rotate -60 -gravity center -crop 520x301+0+0 \
          top_shear.png

  # left image shear
  convert left.png  -background none \
          -shear 0x28  left_shear.png

  # right image shear
  convert right.png  -resize  260x301! -alpha set -background none \
          -shear 0x-30  right_shear.png

  # combine them.
  convert left_shear.png right_shear.png +append \
          \( top_shear.png -repage +0-149 \) \
          -background none -layers merge +repage \
          -resize 30%  isometric_shears.png

  # cleanup
 # rm -f top.png left.png right.png
 # rm -f top_shear.png left_shear.png right_shear.png

 convert -virtual-pixel transparent -interpolate Nearest \
     \( top.png -matte \
        +distort Affine '0,16 0,0   0,0 -16,-8  16,16 16,-8' \) \
     \( left.png -matte \
        +distort Affine '16,0 0,0   0,0 -16,-8  16,16 0,16' \) \
     \( right.png -matte \
        +distort Affine '  0,0 0,0   0,16 0,16    16,0 16,-8' \) \
     -background pink -layers merge +repage +dither -channel alpha -threshold 20% -flatten +matte  -remap ../palette2.bmp -colors 16 BMP3:isometric_cube.bmp


 convert -virtual-pixel transparent -interpolate Nearest -filter point \
     \( top.png -matte \
        +distort Affine '0,16 0,0   0,0 -16,-9  16,16 16,-9' \) \
     \( left.png -matte \
        +distort Affine '16,0 0,0   0,0 -16,-9  16,16 0,16' \) \
     \( right.png -matte \
        +distort Affine '  0,0 0,0   0,16 0,16    16,0 16,-9' \) \
     -background pink -layers merge +repage -remap ../palette2.bmp BMP3:isometric_cube2.bmp
