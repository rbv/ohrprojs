#!/usr/bin/env python

from pymclevel import mclevel
from pymclevel import MCSchematic, extractSchematicFrom
import numpy as np

#world = MCSchematic("netherpalace_rough2.schematic")
#world = mclevel.fromFile("netherpalace_rough2.schematic")

nether = 
box = ((-10, 0, -10), (10, 127, 10))
cutoff = w

world = extractSchematicFrom(nether, box, entities = False)

blocks = world.Blocks

dirt = 3
netherrack = 87
fire = 51
grass = 2
air = 0

print "dirt before:", (world.Blocks == dirt).sum()
print "grass before:", (world.Blocks == grass).sum()

# coords are y,z,x
aboveblocks = blocks[:,:,1:]
belowblocks = blocks[:,:,:-1]
belowblocks[(belowblocks == netherrack) & (aboveblocks != fire)] = dirt
print "dirt after:", (blocks == dirt).sum()
print "grass after:", (blocks == grass).sum()
belowblocks[(belowblocks == dirt) & (aboveblocks == air)] = grass

print "dirt after grassing:", (blocks == dirt).sum()
print "grass after grassing:", (blocks == grass).sum()

world.Blocks = blocks

world.saveToFile("netherpalace2.schematic")
