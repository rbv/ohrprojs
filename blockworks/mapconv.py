#!/usr/bin/env python

from pymclevel import mclevel
from pymclevel.infiniteworld import DIM_NETHER
import numpy as np

block_mapping = dict(zip(range(152), range(152)) +  zip(range(152, 256), [0] * 104))

# deviation from minecraft's

block_mapping[62] = 61 #burning furnace
#wooden stairs
block_mapping[134] = 53
block_mapping[135] = 53
block_mapping[136] = 53

# block_mapping[3] = {0: 200}


blockfaces = [None] * 256
blockfaces[1] = (1,0) #stone
blockfaces[2] = [(3,0),(0,0)] #grass
blockfaces[3] = (3,0) #dirt
blockfaces[4] = (0,1) #cobble
blockfaces[12] = (2,1) #sand



# -crop 9,2,141,149

#ls b*.png | xargs -I {} convert {} -crop 132x147+9+2 +repage -resize 32x32\! -background black -flatten +matte -remap ../palette2.bmp -compress none a_{}


#convert Sand.png -crop 132x147+9+2 +repage -sample 32x32\! -background black -flatten +matte sand2.png

  # convert -virtual-pixel transparent \
  #    \( {} -matte \
  #       +distort Affine '0,0 0,0   149,0  149,0  0,67 0,67' \) \
  #    \( {} -matte \
  #       +distort Affine '0,68 0,68   149,68  149,68  0,149 0,120' \) \
  #    -background black -layers merge +repage \
  #    -bordercolor black -border 5x2    cube3.png


world = mclevel.fromFile("world/")
nether = world.getDimension(DIM_NETHER)


def export_blocks(world, mapno, start, size):
    datascript = """
script, map data %d part %d, begin
  $0="Loading... %d%%"
  show string(0)
  $MAPDATA+"%s"
  #append map data(1)
  wait
end

"""

    mainscript = """
script, map data %d, begin
  # start position %d %d %d
  X WIDTH := %d
  HEIGHT := %d
  Z WIDTH := %d
%s
  show no value
end

"""

    x, y, z = start
    w, h, d = size

    def encode_string(data):
        return "".join("\\x%02x" % block_mapping[ch] for ch in data)
    

    #tiledata = world.Blocks[x:x+w, z:z+d, y:y+h].flatten()
    tiledata = np.zeros((w, d, h))

    outfile = open("mapdata%d.hss" % mapno, "w")

    print "Loading blocks for map", mapno, "..."
    for xi in range(0, w):
        for zi in range(0, d):
            xx = x + xi
            zz = z + zi
            chunk = world.getChunk((x + xi) / 16, (z + zi) / 16)
            tiledata[xi, zi, :] = chunk.Blocks[xx % 16, zz % 16, y:y+h]

    tiledata = tiledata.flatten()

    print (tiledata == 20).sum(), "glass", tiledata[0], tiledata[200], tiledata[400], tiledata[600], tiledata[800]


    print "Writing", len(tiledata), "blocks..."
    partsize = 100000
    numparts = (len(tiledata) + partsize - 1) / partsize
    partlist = ""
    for part in range(numparts):
        partdata = tiledata[part * partsize : (part + 1) * partsize]
        #partdata = partdata.view(('S',len(partdata)))[0]
        percent = int(100.0 * part * partsize / len(tiledata))
        outfile.write(datascript % (mapno, part, percent, encode_string(partdata)))
        partlist += "  map data %d part %d\n" % (mapno, part)

    outfile.write(mainscript % (mapno, x, y, z, w, h, d, partlist))

#export_blocks(world, 0, (-10, 60, -10), (30, 15, 30))
#export_blocks(world, 1, (-355,40,-539), (80,82,80))
#mt james
#export_blocks(world, 1, (-355,0,-539), (80,122,80))

#netherpalace
#export_blocks(nether, 1, (-2817,32,-1064),(245,95,210))
export_blocks(nether, 1, (-2817,32,-1064),(245,60,210))

# #creeperton
# export_blocks(world, 2, (10,53,186), (140,22,170))

# #cathedral
# export_blocks(world, 3, (861,58,464), (182,71,181))

# #NJ fort
# export_blocks(world, 4, (708,63,557), (109,49,102))

# #sheepco
# export_blocks(world, 5, (561,63,2044), (140,37,160))

# #NJ town
# export_blocks(world, 6, (735,70,486), (115,49,70))

# #Ox
# export_blocks(world, 7, (413,63,489), (140,103,151))
