include, plotscr.hsd
include, scancodes.hsi

# For each of the 8 heroes 0-7, returns true or false
# In this demo heros 0, 1, 5 are present, the others aren't
script, hero in party, num, begin
    switch(num) do(
        case(0, 1, 5)
            return(true)
        else
            return(false)
    )
end

# Arrays of globals
defineconstant(100, global:stat bonuses)
defineconstant(120, global:available skills)   # Variable length. Don't know max length
defineconstant(150, global:skill list)   # length = 4 slots * 8 heroes

defineconstant(1, str:skillname)
defineconstant(2, str:skilldescription)

defineconstant(0, slot:default skill)  # not stored?
defineconstant(1, slot:offensive)
defineconstant(2, slot:defensive)
defineconstant(3, slot:passive)


################################################################ Skill scripts

# These scripts describe skills. But it would probably be easier
# to store all these information in item name/description/bonuses

script, aboutskill:pow, begin
    $ str:skillname = "Pow!"
    $ str:skilldescription = "Sucker-punch to the face!"
    # bonuses go here
end

script, aboutskill:flying kick, begin
    $ str:skillname = "Flying kick"
    $ str:skilldescription = "Absurd disregard for physics"
    # bonuses go here
end

script, aboutskill:skill 3, begin
    $ str:skillname = "skill 3"
    $ str:skilldescription = "Description."
    # bonuses go here
end

script, aboutskill:Punching bag, begin
    $ str:skillname = "Punching bag"
    $ str:skilldescription = "You are a magnet for hits, but you can take them easily."
    # bonuses go here
end

script, aboutskill:good looks, begin
    $ str:skillname = "Good looks"
    $ str:skilldescription = "Pretty boy!"
    # bonuses go here
end

script, aboutskill:crit hit, begin
    $ str:skillname = "Crit.Hit."
    $ str:skilldescription = "5% extra damage!"
    # bonuses go here
end


################################################################ Hero scripts

# These scripts work out and list the skills that are enabled
# in each skill slot for each hero

# For each skill slot, list the available skills,
# depending any kind of conditions: tags, items, levels, etc
script, heroskills:joe, slot, begin
    switch (slot) do (
        case(slot:default skill)
            addskill(aboutskill:crit hit)
        case(slot:A)
            addskill(aboutskill:pow)
            addskill(aboutskill:flying kick)
            addskill(aboutskill:skill 3)
        case(slot:B)
            addskill(aboutskill:pow)
        case(slot:passive)
            addskill(aboutskill:punching bag)
            addskill(aboutskill:good looks)
    )
end

# Call the right heroskills:* script... all heros are the same currently
# Fills array:availableskills with all skills the player can currently select
# for this slot.
script, get available skills, hero, slot, begin
    # Clear the list
    writeglobal(array:available skills, 0)

    switch (hero) do (
        case(0)   heroskills:joe(slot)
        # ...
        else      heroskills:joe(slot)
    )
end

################################################################ Helper scripts

# Placeholder: Probably a better way to store skills... like actual equipment.

script, getskill, hero, slot, begin
    return(readglobal(array:skill list + hero * 4 + slot))
end

script, setskill, hero, slot, value, begin
    writeglobal(array:skill list + hero * 4 + slot, value)
end

# Given the start of a list of globals, return id of the first
# global containing 0.
script, listend, ptr, begin
    while (readglobal(ptr)) do (ptr += 1)
    return (ptr)
end

script, add_skill_to_list, skillscript, begin
    variable(ptr)
    # Find the end of the array
    ptr := array:available skills
    while (readglobal(ptr) <> 0) do (ptr += 1)
    writeglobal(ptr, skillscript)
    writeglobal(ptr + 1, 0)
end

################################################################ Menu scripts

plotscript, skills menu toplevel, begin
    variable(collection)
    collection := load slice collection(0)

    # hero_row/column is the currently selected hero.
    # button num is 0 for 'stats' button,
    # 1-3 for the 3 skills
    variable(hero_row, hero_column, button_num)

    while(true) do( 
        if (keyval(key:left)) then ()

        if (keyval(key:esc)) then (break)

        # controls:
        # press Use when selecting 'Stats' to open a window
        # showing stats.

        # press use when selecting a skill to select it,
        # open windows to show the stats and skill description,
        # then left/right change the skill

        wait
    )

    freeslice(collection)
end

script, skill selector menu, hero, slot, begin
    variable(statwindow, skilldescription)
    statwindow := load slice collection(1)

    get available skills(hero, slot)

    variable(skill, ptr)
    skill := getskill(hero, slot)

    # Find the position of the current skill in the list
    # If the current skill is NONE, then this will point to the end of the list.
    # FIXME: what if the current skill is now disabled and not in the available list?
    skillptr := array:available skills
    while (readglobal(skillptr) <> skill && readglobal(skillptr)) do (skillptr += 1)

    subscript, update, begin
        skill := readglobal(skillptr)
        setskill(hero, slot, skill)
    end

    while (true) do (
        if (keyval(key:left) > 1) then (
            if (skillptr == array:available skill) then (
                # Point to the None at the end
                skillptr := listend(array:available skills)
            ) else (
                skillptr -= 1
            )
            update
        ) else if (keyval(key:right) > 1) then (
            if (skill) then (
                skillptr += 1
            ) else (
                skillptr := array:available skills
            )
            update
        )

        if (keyval(key:esc) > 1) then (break)

        wait
    )

    freeslice(statwindow)
end

