;An emacs lisp script to process a log file with lines in reverse order
;into a HS script. A little bit of hand editting required
;Assumes each line starts with a timestamp of form "[XX:XX] <?Name>"

;taken from replace.el and modified to return an integer
(defun how-many- (regexp &optional rstart rend)
  "Print number of matches for REGEXP following point.

If REGEXP contains upper case characters (excluding those preceded by `\\'),
the matching is case-sensitive.

Second and third arg RSTART and REND specify the region to operate on.

Interactively, in Transient Mark mode when the mark is active, operate
on the contents of the region.  Otherwise, operate from point to the
end of the buffer."

  (interactive
   (keep-lines-read-args "How many matches for (regexp): "))
  (save-excursion
    (if rstart
	(goto-char (min rstart rend))
      (if (and transient-mark-mode mark-active)
	  (setq rstart (region-beginning)
		rend (copy-marker (region-end)))
	(setq rstart (point)
	      rend (point-max-marker)))
      (goto-char rstart))
    (let ((count 0)
	  opoint
	  (case-fold-search (and case-fold-search
				 (isearch-no-upper-case-p regexp t))))
      (while (and (< (point) rend)
		  (progn (setq opoint (point))
			 (re-search-forward regexp rend t)))
	(if (= opoint (point))
	    (forward-char 1)
	  (setq count (1+ count))))
      count)))

;    (goto-char (point-min))
;    (while (search-forward "] <@" nil t)
;      (replace-match "] " nil t))
;    (goto-char (point-min))
;    (while (search-forward "] <+" nil t)
;      (replace-match "] " nil t))
;    (goto-char (point-min))
;    (while (search-forward "] <" nil t)
;      (replace-match "] " nil t))


(defun blarg-log ()
  ""
  (interactive)
  (widen)
  (save-excursion
    (goto-char (point-min))
    (while (search-forward "\\" nil t)
      (replace-match "\\\\" nil t))
    (goto-char (point-min))
    (while (search-forward "\"" nil t)
      (replace-match "\\\"" nil t))
    (goto-char (point-min))
    (while (search-forward "include" nil t)
      (replace-match "in\clude" nil t))
    (goto-char (point-min))
    (replace-string "] <@" "] ")
    (goto-char (point-min))
    (replace-string "] <+" "] ")
    (goto-char (point-min))
    (replace-string "] <" "] ")
    
    
    (goto-char (point-min))
    (let ((output "") (lasttime -1))
      (while (< (point) (point-max))



	(let ((time) (temp (thing-at-point 'line)))
	  (setq time (+ (* (+ (* (string-to-number (substring temp 1 2)) 600)
			      (* (string-to-number (substring temp 2 3)) 60)
			      (* (string-to-number (substring temp 4 5)) 10)
			      (string-to-number (substring temp 5 6)))
			   60)
			(/
			 (* 60 (how-many- (concat "^\\" (substring temp 0 6))))
			 (+ (how-many- (concat "^\\" (substring temp 0 6)) (point-min) (point-max))
			    1))))

	  (setq output (concat output
			       "  if (t >= "
			       (number-to-string time)
			       ") then ($1=\""
;			       (substring temp 0 8)
			       (substring temp 0 (- (length temp) 1))
			       "\", lasttime := "
			       (number-to-string lasttime)
			       ", break(needbreak) )\n"))
	  (setq lasttime time))
	(forward-line))
      (goto-char (point-max))
    (insert output))))
