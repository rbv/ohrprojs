

# rescale to height 100, and paste on top of a 320x200 blue screen
# ! indicates don't preserve aspect ratio

texdir=tex/mayhem_skyboxes/
texnames="grouse_lf.jpg grouse_bk.jpg grouse_rt.jpg grouse_ft.jpg"
mainwidth=1024

outdir=generated.tmp/skybox_sprites

mkdir -p $outdir


# crop to 1024x640, y from 0 to 640

i=0
for tex in $texnames; do 
    convert $texdir/$tex -crop 1024x640+0+0 +repage -scale 320x200 -dither FloydSteinberg -remap pal.bmp $outdir/$i.bmp
    ((i++))
done

