
Early Version! So much unimplemented, like variable height floors and walls
and collision checking...
This is the second preview, with a grid-based map and movement.

The scripts are placed in the public domain.

Both free movement and grid movement are implemented and could hopefully be
switched between by changing a couple lines of the scripts.

Textures need to be converted to a set of backdrops using scalesprites.sh and
then mass-imported (using macro keys Ctrl-F11 is fastest).  But now that
backdrops can be larger than 320x200, I would switch to that instead.

*** Controls ***

ASDW: walk, strafe
Arrows: walk, rotate
Z: shoot

Most debug functions are accessed by setting tags with F4.
"label vertices": show info on screen
"split columns": smoother wall tops and bottoms, but slower, and artefacts at wall centres.
"proper projections" and "perspective tex map": turning these off uses wrong projection
    calculations, breaking moving through walls


*** Resources ***

The wall textures and map are stolen from Festivus by Feenick.

The skybox is Mayhem by Calinou
CC-BY 3.0
http://opengameart.org/content/mayhems-skyboxes-more

Other textures in the .rpgdir are not used unless you reenable the old
non-grid test map in the scripts:

The rocky wall texture: public domain
http://www.burningwell.org/

The other wall texture:
Stolen from Valigarmander, don't know where he got it.

The plasmaball sprite is ripped from Doom.
