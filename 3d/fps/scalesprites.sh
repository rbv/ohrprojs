#!/bin/bash
#
# This script creates scaled versions of the wall textures
#


# rescale to height 100, and paste on top of a 320x200 blue screen
# ! indicates don't preserve aspect ratio

BACKINGCOL=blue

# originally 143x86
texname=val_wall.png
mainwidth=143

texname=tex/P1170590_piece.jpg
mainwidth=155

texname=festivus_wall1.bmp
outdir=scaledtex_fest1
mainwidth=140
ALPHA="-transparent rgb(248,24,184)"
BACKINGCOL=black

texname=festivus_wall2.bmp
outdir=scaledtex_fest2
mainwidth=140
# is on a background
ALPHA="-transparent rgb(248,24,184)"
BACKINGCOL=black

#convert ~/ohr/games/festivus/festivus0005.bmp  -crop 140x164 temp.bmp
#mv temp-0.bmp festivus_wall2.bmp 
#rm temp*




mkdir -p $outdir


#for ((height=10;height<=200;height+=10)); do



step=40
for height in $(seq 4 $step 42); do
    outfile=$outdir/$texname_$(printf "%03d" $height).bmp

    convert -size 320x200 xc:$BACKINGCOL $outdir/tmp.bmp

    add_row () {
        x=0
        y=$1
        for colnum in $(seq 1 $per_row); do
            echo "height = " $height
            convert $texname $ALPHA -resize ${width}x$height\!  $outdir/tmp.bmp +swap -geometry +$x+$y            -composite $outdir/tmp.bmp
            ((height = height + rowstep))
            ((x = x + width))
        done
    }

    # Scaling original 143x86 to width of 64 is 64x38
    per_row=5
    per_col=4
    rowstep=$((step/per_row/per_col))
    width=$((320/per_row))  #64
    y=0
    for rownum in $(seq 1 $per_col); do
        add_row $y $((step/per_col))
        ((y = y + 200/per_col))
    done

    mv $outdir/tmp.bmp $outfile
done

step=8
for height in $(seq 44 $step 200); do
    ((height2 = height + $step/2))
    echo "height = " $height
    convert $texname $ALPHA -resize ${mainwidth}x$height\!   -size 320x200 xc:$BACKINGCOL +swap -composite $outdir/tmp.png
    echo "height = " $height2
    convert $texname  $ALPHA -resize ${mainwidth}x$height2\!  $outdir/tmp.png +swap -geometry +160+0 -composite $outdir/$texname_$(printf "%03d" $height).bmp
    rm $outdir/tmp.png
done

# Versions higher than 200 pixels

step=20
for height in $(seq 204 $step 434); do
    ((height2 = height + $step/2))
    # height=$(printf "%03d" $height)
    # height2=$(printf "%03d" $height2)
    ((yoff = (200 - height)/2))
    echo "height = " $height
    convert $texname $ALPHA -resize ${mainwidth}x$height\!   -size 320x200 xc:$BACKINGCOL +swap -geometry +0+$yoff -composite $outdir/tmp.png
    echo "height = " $height2
    ((yoff2 = (200 - height2)/2))
    convert $texname $ALPHA -resize ${mainwidth}x$height2\!  $outdir/tmp.png +swap -geometry +160+$yoff2 -composite $outdir/$texname_$(printf "%03d" $height).bmp
    rm $outdir/tmp.png
done

