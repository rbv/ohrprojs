# Part of:
#             3rd Party Include File                          |
# version 1.92 (Last updated 13/05/2017)                        |

#=====================#
#Operator  Definitions#
#=====================#

define operator, begin   #operators for your use, eg  '50 + 100, times sine, 50' 
  25, times sin, sin wrap 
  25, times cos, cos wrap 
  25, times tan, tan wrap 
  30, div, divideround  
end 

# Compute n/m and round the result
script, divideround, n, m, begin 
  return ((n + sign(n) * sign(m) * m / 2) / m)
end

# Compute sin(angle_in_degrees) * mult
script, sine, angle, mult = 10000, begin 
  variable (val, sgn) 

  if (angle < 0) then (angle := abs(angle), sgn := 1) # sine is an odd function
  sgn := (sgn + angle / 180), mod, 2 
  angle := angle, mod, 180               #this part converts the angle to a value between 0 and 90. 
  if (angle > 90) then (angle := 180 -- angle) 

  if (angle == 90) then (
    val := mult
  ) else (

    if (angle < 45) then ( 
      if (angle < 22) then ( 
        val := sina (angle) 
      ) else ( 
        val := sinb (angle) 
      ) 
    ) else (
     if (angle < 67) then ( 
        val := sinc (angle) 
      ) else ( 
        val := sind (angle) 
      ) 
    )
    val := multdiv(val, mult, 32768)
  )
  if (sgn) then (return (0 -- val)) else (return (val))
end

script, cosine, angle, mult = 10000, begin 
  return (sine (angle + 90, mult)) 
end 

# Multiple two numbers, return 2147483647 or -2147483647 if it overflows
script, bounded multiply, a, b, begin
  variable(ret)
  ret := a * b
  if (b <> 0 && ret / b <> a) then (
    return (2147483647 * sign(a) * sign(b))
  ) else (
    return (ret)
  )
end

# Calculate the norm (length/magnitude) of a 2D vector without overflowing.
# Overflows only if the result should be >= 2^31
script, norm2d, x, y, begin
  x := abs(x)  # Assume not -2^31
  y := abs(y)
  if (x <= 32767 && y <= 32767) then (
    exit (sqrt(x^2 + y^2))
  )

  variable (top, bottom, a3)
  if (x > y) then (top := x, bottom := y) else (top := y, bottom := x)

  # Let:
  # a = bottom / top
  # Then
  # sqrt(top^2 + bottom^2) = top * sqrt(1 + a^2)

  # a3 = a * 30000
  a3 := multdiv(3 0000, bottom, top)

  return (multdiv(top, sqrt(9 0000 0000 + a3 ^ 2), 3 0000))
end

# max error about 9% for certain values of mult like 700 or really large,
# but normally more like 1% or less (eg for 1000)
script, tan, angle, mult = 10000, begin
  variable(sgn, numerator, denominator, ret, temp)

  angle := angle, mod, 180
  if (angle < 0) then (angle := 0 -- angle, sgn := 1)
  if (mult < 0) then (mult := 0 -- mult, sgn := sgn, xor, 1)
  if (angle > 90) then (angle := 180 -- angle, sgn := sgn, xor, 1)

  if (angle == 0) then (exit returning (0))
  if (angle == 90) then (exit returning (2147483647))

  numerator := sine(angle, 32768)
  denominator := sine(angle + 90, 32768)

  temp := 1000 * numerator / denominator
  #r =  bounded_multiply(temp, mult)
  ret := temp * mult
  # Check for overflow
  if (temp <> 0 && ret / temp <> mult) then (
    # mult was > ~37000
    ret := bounded multiply((mult + 250) / 500, temp / 2)
  ) else (
    ret := ret / 1000
  )

  if (sgn) then (
    return (0 -- ret)
  ) else (
    return (ret)
  )
end

script, sina, angle, begin 
  if (angle < 11) then ( 
   if (angle < 5) then ( 
    if (angle < 2) then ( 
     if (angle == 0) then (return (0)) else (return (572)) 
    ) else ( 
     if (angle == 2) then (return (1144)) else ( 
      if (angle == 3) then (return (1715)) else (return (2286)) 
     ) 
    ) 
   ) else ( 
    if (angle < 8) then ( 
     if (angle == 5) then (return (2856)) else ( 
      if (angle == 6) then (return (3425)) else (return (3993)) 
     ) 
    ) else ( 
     if (angle == 8) then (return (4560)) else ( 
      if (angle == 9) then (return (5126)) else (return (5690)) 
     ) 
    ) 
   ) 
  ) else ( 
   if (angle < 16) then ( 
    if (angle < 13) then ( 
     if (angle == 11) then (return (6252)) else (return (6813)) 
    ) else ( 
     if (angle == 13) then (return (7371)) else ( 
      if (angle == 14) then (return (7927)) else (return (8481)) 
     ) 
    ) 
   ) else ( 
    if (angle < 19) then ( 
     if (angle == 16) then (return (9032)) else ( 
      if (angle == 17) then (return (9580)) else (return (10126)) 
     ) 
    ) else ( 
     if (angle == 19) then (return (10668)) else ( 
      if (angle == 20) then (return (11207)) else (return (11743)) 
     ) 
    ) 
   ) 
  ) 
end 

script, sinb, angle, begin 
  if (angle < 33) then ( 
   if (angle < 27) then ( 
    if (angle < 24) then ( 
     if (angle == 22) then (return (12275)) else (return (12803)) 
    ) else ( 
     if (angle == 24) then (return (13328)) else ( 
      if (angle == 25) then (return (13848)) else (return (14365)) 
     ) 
    ) 
   ) else ( 
    if (angle < 30) then ( 
     if (angle == 27) then (return (14876)) else ( 
      if (angle == 28) then (return (15384)) else (return (15886)) 
     ) 
    ) else ( 
     if (angle == 30) then (return (16384)) else ( 
      if (angle == 31) then (return (16877)) else (return (17364)) 
     ) 
    ) 
   ) 
  ) else ( 
   if (angle < 39) then ( 
    if (angle < 36) then ( 
     if (angle == 33) then (return (17847)) else ( 
      if (angle == 34) then (return (18324)) else (return (18795)) 
     ) 
    ) else ( 
     if (angle == 36) then (return (19261)) else ( 
      if (angle == 37) then (return (19720)) else (return (20174)) 
     ) 
    ) 
   ) else ( 
    if (angle < 42) then ( 
     if (angle == 39) then (return (20622)) else ( 
      if (angle == 40) then (return (21063)) else (return (21498)) 
     ) 
    ) else ( 
     if (angle == 42) then (return (21926)) else ( 
      if (angle == 43) then (return (22348)) else (return (22763)) 
     ) 
    ) 
   ) 
  ) 
end 

script, sinc, angle, begin 
  if (angle < 56) then ( 
   if (angle < 50) then ( 
    if (angle < 47) then ( 
     if (angle == 45) then (return (23170)) else (return (23571)) 
    ) else ( 
     if (angle == 47) then (return (23965)) else ( 
      if (angle == 48) then (return (24351)) else (return (24730)) 
     ) 
    ) 
   ) else ( 
    if (angle < 53) then ( 
     if (angle == 50) then (return (25102)) else ( 
      if (angle == 51) then (return (25466)) else (return (25822)) 
     ) 
    ) else ( 
     if (angle == 53) then (return (26170)) else ( 
      if (angle == 54) then (return (26510)) else (return (26842)) 
     ) 
    ) 
   ) 
  ) else ( 
   if (angle < 61) then ( 
    if (angle < 58) then ( 
     if (angle == 56) then (return (27166)) else (return (27482)) 
    ) else ( 
     if (angle == 58) then (return (27789)) else ( 
      if (angle == 59) then (return (28088)) else (return (28378)) 
     ) 
    ) 
   ) else ( 
    if (angle < 64) then ( 
     if (angle == 61) then (return (28660)) else ( 
      if (angle == 62) then (return (28932)) else (return (29197)) 
     ) 
    ) else ( 
     if (angle == 64) then (return (29452)) else ( 
      if (angle == 65) then (return (29698)) else (return (29935)) 
     ) 
    ) 
   ) 
  ) 
end 

script, sind, angle, begin 
  if (angle < 78) then ( 
   if (angle < 72) then ( 
    if (angle < 69) then ( 
     if (angle == 67) then (return (30163)) else (return (30382)) 
    ) else ( 
     if (angle == 69) then (return (30592)) else ( 
      if (angle == 70) then (return (30792)) else (return (30983)) 
     ) 
    ) 
   ) else ( 
    if (angle < 75) then ( 
     if (angle == 72) then (return (31164)) else ( 
      if (angle == 73) then (return (31336)) else (return (31499)) 
     ) 
    ) else ( 
     if (angle == 75) then (return (31651)) else ( 
      if (angle == 76) then (return (31795)) else (return (31928)) 
     ) 
    ) 
   ) 
  ) else ( 
   if (angle < 84) then ( 
    if (angle < 81) then ( 
     if (angle == 78) then (return (32052)) else ( 
      if (angle == 79) then (return (32166)) else (return (32270)) 
     ) 
    ) else ( 
     if (angle == 81) then (return (32365)) else ( 
      if (angle == 82) then (return (32449)) else (return (32524)) 
     ) 
    ) 
   ) else ( 
    if (angle < 87) then ( 
     if (angle == 84) then (return (32588)) else ( 
      if (angle == 85) then (return (32643)) else (return (32688)) 
     ) 
    ) else ( 
     if (angle == 87) then (return (32723)) else ( 
      if (angle == 88) then (return (32748)) else (return (32763)) 
     ) 
    )
   )
  )
end 

script, sin wrap, mult, angle (return (sine (angle, mult))) 
script, cos wrap, mult, angle (return (cosine (angle, mult))) 
script, tan wrap, mult, angle (return (tan (angle, mult)))

# Result in thousands of a radian, between 3142 and -3142.
# Original floating point version had |error| < 5 mRad, this would be more than that.
script, atan2 milliradians, y, x, begin
  variable(atan, z, pi)
  pi := 3142
  if (x == 0) then (
    if (y > 0) then (return(pi/2))
    else if (y == 0) then (return(0))
    else (return(pi/-2))
  )
  else(
    z := 1000 * y / x
    if (abs(z) < 1000)
    then(
      atan := 1000*z/(1000 + z*z*28/100/1000)
      if (x < 0) then(
        if (y < 0) then (exitreturning(atan -- pi))
        exitreturning(atan + pi)
      )
    )
    else(
      if (abs(z) > 65000) then(
        # prevent overflow if z > sqrt(2 * 2**31)
        atan := pi/2
      )
      else(
        atan := pi/2 -- 1000*z/(z/2*z/500 + 280)  # pi/2 - z/(z*z + 0.28)
      )
      if (y < 0) then (exitreturning(atan -- pi))
    )
    return(atan)
  )
end

# Result in degrees. Returns mult*atan(y,x). Probably accurate to about half a degree.
# Returns a value between 180 and -180:
#  180: left
#  90: up
#  0: right
#  -90: down
#  -180: left
script, atan2, y, x, mult = 1, begin
  return (divideround(atan2 milliradians(y, x) * mult * 180, 3142))  # *180/pi
end

# Returns max(1, e^(n/1000) * 1000): always returns a positive value
# Delete the +1 at the end if you want it to reach 0
# For n > 0, maximum error in the result is 0.3%
script, exp1000, n, begin
  if (n >= 7291) then (
    if (n >= 14579) then (
      # Prevent overflow
      exitreturning(2145750645)
    )
    # exp(7.291) = 1467.04
    exitreturning(exp1000(n -- 7291) * 1467)
  )
  variable(mult, val)
  mult := 1000
  while (n <= -414) do (
    # exp(-0.414) = 0.661001
    mult := mult * 661 / 1000
    n += 414
  )
  while (n >= 540) do (
    # exp(0.540) = 1.716007
    mult := mult * 1716 / 1000
    n -= 540
  )

  #val := 1 + n + n**2 / 2 + n**3 / 6 + n**4 / 24
  val := 1000 + n + n^2 / 2000 + n^3 / 6000000 + (n^3 / 1000) * n / 24000000
  return(val * mult / 1000 + 1)
end
