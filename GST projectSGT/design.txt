https://docs.google.com/document/d/1X68L0UY8-Ec26DPu43lzyj1VMwx1LqjpY-_2xlOewl8/pub
ProjectSGT

LOSERTOWN PROLOGUE

Losertown is situated in a steep crater that is impossible to climb out of. As a result it�s mostly used to dump societal rejects or failures into to get them out of everyone�s hair. It�s also a popular way to dispose of unpopular warlords, either by dumping their families into it to lure them down, or by cutting off their arms and throwing them in the pit for a stronger sense of disempowerment.

(2,0,0): Crash Site

The player�s drop pod lands here.

LOSERTOWN STORY

Losertown Attitude: Despite all being outsiders themselves once, strangers tend to be ostracized from the town unless they can prove their worth. Coldhearted on the outside, this is the result of the countless thugs thrown into the pit who try to stir up trouble as though they were back outside. Often the townspeople will come together to murder any large outside groups before they can form in order to prevent any uprisings. The result is a tight knit town that is extremely cold to most outsiders, often directing them to dangerous places like the cave to get rid of them cleanly.

Robot Arm Man: was another deposed warlord who had his arms chopped off. Upon his arrival a stranger who landed on the planet with a robot arm was willing to give him his arm. Touched by kindness for the first time in his life, the warlord dedicated himself to starting a farm and providing for loser town�s helpless.

Over time he developed a sense of ownership over losertown. When he discovered a cave leading out of it he did everything in his power to prevent the others from discovering it: hiding the entrance, breeding monsters inside of it, and placing many traps throughout it. The few people aware of the cave assume it to be a deathtrap.

Charles: Separated from his family when he was thrown into Losertown, he refused to give up on being reunited with them. This attitude clashed with Robot Arm Man, though he couldn�t show it publicly. Charles suspected someone was actively preventing him and hid notes around the area about his findings to keep suspicion off of him while he was out hunting. Eventually Robot Arm Man cornered him inside the cave and killed him before he could leave. He drug his body outside the cave and let the monsters chew on his bones so that the others would believe it to be too dangerous of a place to explore.

One Armed Man: Former engineer thrown in the pit alongside Robot Arm Man. Gave him his robotic arm to win the heart of his kindhearted wife. Ever since she passed away he�s grown to regret the decision despite its many benefits. He usually takes it out on any newcomers, since she was killed in the cave while helping Charles.
