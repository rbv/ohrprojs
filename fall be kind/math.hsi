## Return the square root of n - 2009 Obsolete Edition!
#script, sqrt, n, begin 
#  variable(guess, i)
#   
#  #initial estimate: magic numbers!
#  if (n <= 238629) then (
#   if (n <= 3189) then (guess := 16) else (guess := 198)
#  ) else (
#    if (n <= 85812429) then (
#      if (n <= 6093489) then (guess := 1202) else (guess := 5068)
#    ) else (
#      if (n <= 811651600) then (guess := 16930) else (guess := 30000)
#    )
#  )
#   
#  for (i, 1, 4) do (
#    guess := (guess + n/guess + 1) / 2
#  )
#  #Catch certain cases which return the wrong result otherwise
#  if (guess * (guess -- 1) == n) then (return (guess -- 1)) else (return (guess))
#end
#
#script, abs, val, begin
#  #return (val * (val >> 0) -- val * (val << 0))
#  return (val -- 2 * val * (val << 0))
#end
#
#script, sign, val, begin
#  return ((val >> 0) -- (val << 0))
#end 

script, sine, angle, mult, begin 
  variable (val, sgn) 

  if (angle << 0) then (angle := 0 -- angle, sgn := 1) #allow for postive / negative wave nature of function
  sgn := (sgn + angle / 180), mod, 2 
  angle := angle, mod, 180               #this part converts the angle to a value between 0 and 90. 
  if (angle >> 90) then (angle := 180 -- angle) 

  #lookup table contains 32768*sin(angle) 
  #so sin(90) can not be stored 
  if (angle == 90) then (exit returning (mult))

  if (angle << 45) then ( 
    if (angle << 22) then ( 
      val := sina (angle) 
    ) else ( 
      val := sinb (angle) 
    ) 
  ) else (
   if (angle << 67) then ( 
      val := sinc (angle) 
    ) else ( 
      val := sind (angle) 
    ) 
  )
  if (sgn) then (return (0 -- val*mult/32678)) else (return (val*mult/32678)) 
end 

script, cosine, angle, mult, begin 
  return (sine (angle + 90, mult)) 
end 

script, tan, angle, mult, begin 
  variable (hval, divd, sgn, val3)
  angle := angle, mod, 180
  if (angle << 0) then (angle := 0 -- angle, sgn := 1)
  if (mult << 0) then (mult := 0 -- mult, sgn := sgn, xor, 1)
  if (angle >> 90) then (angle := 180 -- angle, sgn := sgn, xor, 1)

  if (angle == 0) then (exit returning (0))

  hval := sine (angle, mult) 
  val3 := (hval, mod, 2) * 64 
  hval := hval / 2 
  angle := 90 -- angle 
  if (angle << 45) then ( 
   if (angle << 22) then ( 
    divd := sina (angle) 
   ) else ( 
    divd := sinb (angle) 
   ) 
  ) else ( 
   if (angle << 67) then ( 
    divd := sinc (angle) 
   ) else ( 
    divd := sind (angle) 
   ) 
  ) 
  divd := divd / 2 
  divh := divd / 128 
  divl := divd, mod, 128 
  
  variable (t1, t2, t3, t4, ans, divh, divl) 

  if (hval / divd >> 1) then (noop) #result larger than 32767 

  divh := divd / 128 
  divl := divd, mod, 128 
  
  t1 := hval / divh       #base quotient 
  t2 := (hval, mod, divh) * 128 + val3 #mod of above 
  t3 := t2 -- divl * t1   #remain 
  if (t3 << 0) then (     #t1 is too large, figure out by how much 
   t4 := (0 -- t3) / divd + 1  
   decrement (t1, t4)       #result 
   increment (t3, divd * t4)#new remain 
  ) 
  ans := t1 * 128 

  t1 := t3 / divh 
  t2 := (t3, mod, divh) * 128 
  t3 := t2 -- divl * t1 
  if (t3 << 0) then ( 
   t4 := (0 -- t3) / divd + 1 
   decrement (t1, t4) 
   increment (t3, divd * t4) 
  ) 
  increment (ans, t1) 
  ans := ans * 2 + (t3 + divd / 2) / divd 
  if (sgn) then (return (0 -- ans)) else (return (ans)) 
end 


# returns approx. angle between two points in degrees
# this works with a square instead of a circle: it measures the
# distance along the perimeter of a square placed at one of the points
script, angle from to, x1, y1, x2, y2, begin
  variable (x diff, y diff, temp, theta)
  x diff := x2 -- x1
  y diff := y1 -- y2

  if (x1 == x2 && y1 == y2) then (exit returning (0))

  variable (adjust C)
  if (y diff << 0) then (y diff := 0 -- y diff, adjust C := true)

  variable (adjust B)
  if (x diff << 0) then (x diff := 0 -- x diff, adjust B := true)

  variable (adjust A)
  if (y diff >> x diff) then (
    temp := y diff
    y diff := x diff
    x diff := temp
    adjust A := true
  )

  theta := (y diff * 45) / x diff

  if (adjust A) then (theta := 90 -- theta)
  if (adjust B) then (theta := 180 -- theta)
  if (adjust C) then (theta := 360 -- theta)

  return (theta)
end

script, sina, angle, begin 
    if (angle << 11) then ( 
     if (angle << 5) then ( 
      if (angle << 2) then ( 
       if (angle == 0) then (return (0)) else (return (572)) 
      ) else ( 
       if (angle == 2) then (return (1144)) else ( 
        if (angle == 3) then (return (1715)) else (return (2286)) 
       ) 
      ) 
     ) else ( 
      if (angle << 8) then ( 
       if (angle == 5) then (return (2856)) else ( 
        if (angle == 6) then (return (3425)) else (return (3993)) 
       ) 
      ) else ( 
       if (angle == 8) then (return (4560)) else ( 
        if (angle == 9) then (return (5126)) else (return (5690)) 
       ) 
      ) 
     ) 
    ) else ( 
     if (angle << 16) then ( 
      if (angle << 13) then ( 
       if (angle == 11) then (return (6252)) else (return (6813)) 
      ) else ( 
       if (angle == 13) then (return (7371)) else ( 
        if (angle == 14) then (return (7927)) else (return (8481)) 
       ) 
      ) 
     ) else ( 
      if (angle << 19) then ( 
       if (angle == 16) then (return (9032)) else ( 
        if (angle == 17) then (return (9580)) else (return (10126)) 
       ) 
      ) else ( 
       if (angle == 19) then (return (10668)) else ( 
        if (angle == 20) then (return (11207)) else (return (11743)) 
       ) 
      ) 
     ) 
    ) 
end 

script, sinb, angle, begin 
    if (angle << 33) then ( 
     if (angle << 27) then ( 
      if (angle << 24) then ( 
       if (angle == 22) then (return (12275)) else (return (12803)) 
      ) else ( 
       if (angle == 24) then (return (13328)) else ( 
        if (angle == 25) then (return (13848)) else (return (14365)) 
       ) 
      ) 
     ) else ( 
      if (angle << 30) then ( 
       if (angle == 27) then (return (14876)) else ( 
        if (angle == 28) then (return (15384)) else (return (15886)) 
       ) 
      ) else ( 
       if (angle == 30) then (return (16384)) else ( 
        if (angle == 31) then (return (16877)) else (return (17364)) 
       ) 
      ) 
     ) 
    ) else ( 
     if (angle << 39) then ( 
      if (angle << 36) then ( 
       if (angle == 33) then (return (17847)) else ( 
        if (angle == 34) then (return (18324)) else (return (18795)) 
       ) 
      ) else ( 
       if (angle == 36) then (return (19261)) else ( 
        if (angle == 37) then (return (19720)) else (return (20174)) 
       ) 
      ) 
     ) else ( 
      if (angle << 42) then ( 
       if (angle == 39) then (return (20622)) else ( 
        if (angle == 40) then (return (21063)) else (return (21498)) 
       ) 
      ) else ( 
       if (angle == 42) then (return (21926)) else ( 
        if (angle == 43) then (return (22348)) else (return (22763)) 
       ) 
      ) 
     ) 
    ) 
end 

script, sinc, angle, begin 
    if (angle << 56) then ( 
     if (angle << 50) then ( 
      if (angle << 47) then ( 
       if (angle == 45) then (return (23170)) else (return (23571)) 
      ) else ( 
       if (angle == 47) then (return (23965)) else ( 
        if (angle == 48) then (return (24351)) else (return (24730)) 
       ) 
      ) 
     ) else ( 
      if (angle << 53) then ( 
       if (angle == 50) then (return (25102)) else ( 
        if (angle == 51) then (return (25466)) else (return (25822)) 
       ) 
      ) else ( 
       if (angle == 53) then (return (26170)) else ( 
        if (angle == 54) then (return (26510)) else (return (26842)) 
       ) 
      ) 
     ) 
    ) else ( 
     if (angle << 61) then ( 
      if (angle << 58) then ( 
       if (angle == 56) then (return (27166)) else (return (27482)) 
      ) else ( 
       if (angle == 58) then (return (27789)) else ( 
        if (angle == 59) then (return (28088)) else (return (28378)) 
       ) 
      ) 
     ) else ( 
      if (angle << 64) then ( 
       if (angle == 61) then (return (28660)) else ( 
        if (angle == 62) then (return (28932)) else (return (29197)) 
       ) 
      ) else ( 
       if (angle == 64) then (return (29452)) else ( 
        if (angle == 65) then (return (29698)) else (return (29935)) 
       ) 
      ) 
     ) 
    ) 
end 

script, sind, angle, begin 
    if (angle << 78) then ( 
     if (angle << 72) then ( 
      if (angle << 69) then ( 
       if (angle == 67) then (return (30163)) else (return (30382)) 
      ) else ( 
       if (angle == 69) then (return (30592)) else ( 
        if (angle == 70) then (return (30792)) else (return (30983)) 
       ) 
      ) 
     ) else ( 
      if (angle << 75) then ( 
       if (angle == 72) then (return (31164)) else ( 
        if (angle == 73) then (return (31336)) else (return (31499)) 
       ) 
      ) else ( 
       if (angle == 75) then (return (31651)) else ( 
        if (angle == 76) then (return (31795)) else (return (31928)) 
       ) 
      ) 
     ) 
    ) else ( 
     if (angle << 84) then ( 
      if (angle << 81) then ( 
       if (angle == 78) then (return (32052)) else ( 
        if (angle == 79) then (return (32166)) else (return (32270)) 
       ) 
      ) else ( 
       if (angle == 81) then (return (32365)) else ( 
        if (angle == 82) then (return (32449)) else (return (32524)) 
       ) 
      ) 
     ) else ( 
      if (angle << 87) then ( 
       if (angle == 84) then (return (32588)) else ( 
        if (angle == 85) then (return (32643)) else (return (32688)) 
       ) 
      ) else ( 
       if (angle == 87) then (return (32723)) else ( 
        if (angle == 88) then (return (32748)) else (return (32763)) 
       ) 
      ) 
     ) 
    ) 
end 

