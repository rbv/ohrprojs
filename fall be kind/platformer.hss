#Modified version of Moogle's SideScroller 101

define constant, begin
  20, first location trigger
  29, last location trigger
  30, first action trigger
  49, last action trigger

  5, wiggle room
end

script, initialise hero, begin
  hero-x := hero pixel x (me) * 10
  hero-y := hero pixel y (me) * 10
  hero slice := create container (14, 20)
  set slice extra (hero slice, TARGETX:hurt script, @hurt hero)
  set parent (hero slice, ally target layer)
  hero-vx := 0
  hero-vy := 0
end

script, initialise platformer, begin
  suspend player
  initialise hero
  gravity := 25
  friction := 40
  hero-accel := 25  # accelation while standing
  hero-jump-accel := 25  # acceleration while in midair
  hero-jump-speed := -145
  hero-max-vx := 80
  hero-max-vy := 160
  hero-hurt-propel := -100 # -60
  hero-direction := right
  hero-shoot-rate := 10
  playing := true
end


#========================================

script, do game, begin

  if (playing == false) then (exit script)

  ##### Player input

  variable (want left, want right, want jump, want shoot)

  if (moving suspended == false) then (
    if (key is pressed (key:right)) then (want right := true)
    if (key is pressed (key:left)) then (want left := true)
    if (key is pressed (key:z)) then (want jump := true)
  )
  if (shooting suspended == false) then (
    if (key is pressed (key:x)) then (want shoot := true)
  )

  # DEBUG KEYS
  if (key is pressed (key:s) && skipdoor <> -1) then (npc door (skip door))
  if (key is pressed (key:d)) then (pick slice (target layer))
  if (key is pressed (key:f)) then (pick slice (ally target layer))
  if (key is pressed (key:g)) then (pick slice (bullet layer))
  if (key is pressed (key:h)) then (pick slice (ally bullet layer))
  if (key is pressed (key:j)) then (pick slice (gfx layer))


  # Accept player input
  if (keyval (key:esc) >> 1) then (playing := false)
  if (want right) then (
    hero-direction := right
    if (hero can jump) then (
      hero-vx += hero-accel
    ) else (
      hero-vx += hero-jump-accel
    )
  )
  if (want left) then (
    hero-direction := left
    if (hero can jump) then (
      hero-vx -= hero-accel
    ) else (
      hero-vx -= hero-jump-accel
    )
  )
  point direction := hero-direction
  if (moving suspended == false) then (
    if (key is pressed (key:up)) then (
      point direction := up
    )
    if (key is pressed (key:down)) then (
      point direction := down
    )
  )
  if (want jump && hero can jump) then (hero-vy := hero-jump-speed, play sound (2))

  # Reduce speed if our hero's going too fast
  if (hero-vy >> hero-max-vy) then (hero-vy := hero-max-vy)
  if (hero-vx >> hero-max-vx) then (hero-vx := hero-max-vx)
  if (hero-vx << hero-max-vx * -1) then (hero-vx := hero-max-vx * -1)

  hero-x += hero-vx
  hero-y += hero-vy


  # TODO: Other game-playing stuff goes here.

  #========================================

  if (hero-invincibility-time) then (hero-invincibility-time -= 1)

  time since shot += 1
  if (want shoot && time since shot >= hero-shoot-rate) then (new player bullet, time since shot := 0)

  update bullets

  check enemy collision

  ai movement

  check npcs
    
  #if (used door) then (

  #)

  HUD

  animate gfx

  #========================================

  hero can jump := false
  if (can fall) then (hero-vy += gravity)
  else (
    if (want jump == false) then (hero can jump := true)
    # Apply friction
    if (hero-vx << friction && hero-vx >> friction * -1
        && want left == false && want right == false)
        then (hero-vx := 0)
    if (hero-vx >= friction && want right == false)
        then (hero-vx -= friction)
    if (hero-vx <= friction * -1 && want left == false)
        then (hero-vx += friction)
  )

  if (hero-vy <= 0) then (can rise)
  if (hero-vx <= 0) then (can left)
  if (hero-vx >= 0) then (can right)
  put hero (me, hero-x/10, hero-y/10)
  put slice (hero slice, hero-x/10 + 3, hero-y/10)
  animate hero

  #loop
  set timer (main loop timer, 0, 1, @do game)

end

script, can fall, begin
  variable (hy) # hero's y-position in maptiles
  hy := hero-y / 200 + 1
  if(
    (read pass block((hero-x / 10 + wiggle room) / 20, hy), and, north wall)
    ||
    (read pass block((hero-x / 10 + 20 -- wiggle room) / 20, hy), and, north wall)
    ||
    (hy >= map height)
  )
  then (
    if (hero-vy>=0)
    then (
      hero-y := hero-y -- (hero-y, mod, 200)
      hero-vy := 0
    )
    return(false)
  )
  else (return(true))
end

#crap
script, npc can fall, sl, begin
  variable (sy)
  sy := slicey(sl) / 20 + 1
  if(
    (read pass block((slicex(sl) + wiggle room) / 20, sy), and, north wall)
    ||
    (read pass block((slicex(sl) + 20 -- wiggle room) / 20, sy), and, north wall)
    ||
    (sy >= map height)
  )
  then (
    if (get vy(sl)>=0)
    then (
      set slice y(sl, slicey(sl) -- (slicey(sl), mod, 20))
      set vy(sl,0)
    )
    return(false)
  )
  else (return(true))
end


#========================================

script, can rise, begin
  variable (hy)
  hy := (hero-y) / 200
  if(
    (read pass block((hero-x / 10 + wiggle room) / 20, hy), and, south wall)
    ||
    (read pass block((hero-x / 10 + 20 -- wiggle room) / 20, hy), and, south wall)
    ||
    (hy<<0)
  )
  then (
    hero-y := hero-y -- hero-y,mod,200 + 200
    if (hero-vy<<0) then (hero-vy:=0)
    return(false)
  )
  else (return(true))
end

#========================================

script, can left, begin
  variable (hx)
  hx := (hero-x--10) / 200
  if(
    (readpassblock(hx,(hero-y) / 200), and, east wall)
    ||
    (readpassblock(hx,(hero-y + 199) / 200), and, east wall)
    ||
    (hx<<0)
  )
  then (
    variable(new x)
    new x := 0
    if (hero-x,mod,200 >> 100) then (newx := 200)
    if (hero-vx << 100, and, (hero-x,mod,200 >> 50)) then (newx := 200)
    hero-x := hero-x -- (hero-x,mod,200) + new x

    if (hero-vx << 0) then (hero-vx := 0)
    return(false)
  )
  else (return(true))
)


script, npc can left, sl, begin
  variable (sx)
  sx := (slice x(sl)--1)/20
  if(
    (readpassblock(sx,(slicey(sl)) / 20), and, east wall)
    ||
    (readpassblock(sx,(slicey(sl) + 19) / 20), and, east wall)
    ||
    (sx<<0)
  )
  then (
    return(false)
  )
  else (return(true))
)

script, walkable distance left, sl, begin
  variable (sx, sy, dist)
  sx := slice x(sl)/20
  dist := slice x(sl),mod, 20
  sy := slice y(sl)/20
  for (sx, sx--1, 0, -1) do (
    if (readpassblock(sx, sy), and, east wall) then (break)
    if (sy+1 << map height) then (
      if (readpassblock(sx, sy+1), and, north wall) else (break)
    )
    dist += 20
  )
  return (dist)
)

#========================================

script, can right, begin
  variable (hx)
  hx:= hero-x / 200 + 1
  if (
    (read pass block(hx, hero-y / 200), and, west wall)
    ||
    (read pass block(hx, (hero-y + 199) / 200), and, westwall)
    ||
    (hx >= map width)
  )
  then (
    hero-x := hero-x -- (hero-x,mod,200)

    if (hero-vx >> 0) then (hero-vx := 0)
    return(false)
  )
  else (return(true))
end

script, walkable distance right, sl, begin
  variable (sx, sy, dist)
  sx := (slice x(sl)+19)/20
  dist := (20 -- (slice x(sl),mod, 20)), mod, 20
  sy := slice y(sl)/20
  for (sx, sx+1, mapwidth--1, 1) do (
    if (readpassblock(sx, sy), and, west wall) then (break)
    if (sy+1 << map height) then (
      if (readpassblock(sx, sy+1), and, north wall) else (break)
    )
    dist += 20
  )
  return (dist)
)

#========================================

script, animate hero, begin
   if (hero-direction == right) then (
      set hero frame(me, 1)
   ) else (
      set hero frame(me, 0)
   )
   
   if (hero-vy << 0) then (
      # Jumping
      set hero direction(me, 2)
   ) else (
      if (hero-vy >> 0) then (
         # Falling
         set hero direction(me, 3)
      ) else (
         if (hero-vx <> 0) then (
            # Cycle between walking and standing
            if (hero-vx << 0) then (hero-animation -= hero-vx)
            if (hero-vx >> 0) then (hero-animation += hero-vx)
            if (hero-animation >= hero-max-vx * 2) then (hero-animation -= hero-max-vx * 2)
            if (hero-animation >> hero-max-vx) then (
               set hero direction(me, 0)
            ) else (
               set hero direction(me, 1)
            )
         ) else (
            # Standing
            set hero direction(me, 0)
         )
      )
   )

   if (hero-invincibility-time) then (
     hero-invincibility-tick := (hero-invincibility-tick + 1), mod, 4
     if ((hero-invincibility-tick,mod,2) == 0) then (
       set hero palette (me, 1 + 7 * (hero-invincibility-tick/2), outside battle)
     )
   ) else (set hero palette(me, 1, outside battle))

   if (hero weapon == WEAPON:BAZOOKA) then ( set hero picture (me, 23))
   else (
   if (point direction == up) then ( set hero picture (me, 8))
   else(
     if (point direction == down) then ( set hero picture (me, 22))
     else (set hero picture (me, 1))
   )
   )
end

#========================================

plotscript, npc door, which, begin

  skip door := -1

    # Player changed maps. Update location variables.
    hero-x := hero pixel x * 10
    hero-y := hero pixel y * 10
    hero-vx := 0
    hero-vy := 0
    used door := false

    #remove all temporary objects and enemies
    free slice (gfx layer)
    free slice (bullet layer)
    free slice (target layer)
    free slice (ally bullet layer)
    #free slice (ally target layer)

    initialise layers

    #hero slice := create container(14,20)
    #set parent (hero slice, ally target layer)

  use door (which)
  used door := true
end

script, npc near hero, ref, begin
  variable(nx, ny)
  nx := npc pixel x(ref) * 10
  ny := npc pixel y(ref) * 10
  return ((nx -- hero-x << 200 && hero-x -- nx << 200) &&
          (ny -- hero-y << 200 && hero-y -- ny << 200))
end


script, check npcs, begin
  variable(i, j, ref)
  # Location triggers
  for (i, first location trigger, last location trigger) do (
    for (j, 0, npc copy count(i) -- 1) do (
      ref := npc reference(i, j)
      if (npc near hero (ref)) then (
        use npc (ref)
        if (current text box <> -1) then (wait for text box)
      )
    )
  )
end
