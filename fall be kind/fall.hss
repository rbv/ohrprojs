# Fall Be Kind

include, plotscr.hsd
include, scancode.hsi

include, platformer.hss
include, graphics.hss
include, bullets.hss
include, aipath.hss
include, npcai.hss
include, misc.hss
include, math.hsi

define constant, begin

  #######  SLICE OBJECTS

  # a 'target' (eg. an enemy) is a slice with at least two children and TARGETX slice extra
  # the first child is automatically animated (GFXX slice extras), assumed to be a sprite
  # the last child is a container holding extra data, DATASLX slice extras
 
  #  Target  (any slice, TARGETX extras)
  #    +--- [firstchild: graphic (sprite, GFXX extras)] (optional, only if a sprite)
  #    +--- ...  (other children user defined)
  #    \--- lastchild: dataslice (any slice, DATASLX extras)

  0, TARGETX:hp
  1, TARGETX:hurt script
  2, TARGETX:pathptr

  # Targets, Animations, and Bullets all have extra data stored in a mandatory
  # 'data slice'. The data slice is always the last child. It is used by
  # setvelocity, slicemotion and the vx/vy convenience commands.

  0, DATASLX:vx
  1, DATASLX:vy
  2, DATASLX:flags #  +2048*32768 if affected by gravity

  # bits of DATASLX:flags
  #1024*32768
  33554432, DATASLBIT:GRAVITY  # apply gravity to vy when updating motion


  #  Animation  (sprite, GFXX extras)
  #    +--- ...  (other children user defined)
  #    \--- [lastchild: dataslice (any slice, DATASLX extras)] (optional, may have no children at all)


  # graphics slices extras
  # Rate is the number of ticks per frame, Tick is modulo Rate
  0, GFXX:FlagsNFramesNFrame  # Flags + Frames*32768 + Frame
  1, GFXX:RateNTick           # Rate*32768 + Ticks to next frame

  # bits that go into GFXX:FlagsNFramesNFrame
  33554431, GFXBITMASK   #used to remove flags 
  33554432, GFXBIT:LOOP  #1024*32768   looping animation (if false, destroyed at en)
  67108864, GFXBIT:PAUSE #2048*32768   pause at end before looping (about trice total animation length)


  # a bullet also has a dataslice. Not animated

  #  Bullet  (any slice, BULLETX extras)
  #    +--- ...  (other children user defined)
  #    \--- lastchild: dataslice (any slice, DATASLX extras)

  0, BULLETX:target
  1, BULLETX:damage


  #######  GLOBALS

  150, ARRAY:BOSS PIECES


  # Executing paths:
  # if DESTX or DESTY >> -1, then walk in direction DIR (which might be -1)
  # when that completes, DESTX and DESTY set to -1
  # then if DIR <> -1, face that direction
  # then count down WAIT until 0
  # then if SCRIPT, call it
  # else, or if the script returns -1, delete path and consider NEXTPTR

  200, ARRAY:PATH
  1000, ARRAYEND:PATH
  10, SIZEOF:PATH
  0, PATH:SEG     # next segment, path piece number for fixed paths, essentially a state variable otherwise
  1, PATH:DEST X
  2, PATH:DEST Y
  3, PATH:SPEED
  4, PATH:WAIT    # number of ticks to wait after finished segment actions befor starting next
  5, PATH:ABSOLUTE
  6, PATH:SCRIPT
  7, PATH:NEXTPTR  # ptr to next path object segment in queue, -1 for none, 0 for path not used
  8, PATH:EXTRA1
  9, PATH:EXTRA2

  #######  TIMERS

  14, main loop timer
  15, rain timer

  #######  STRINGS  (FIXME: surely must be more?)

  31, temp string

  #######  SETTINGS



  ####

  0, WEAPON:PISTOL
  1, WEAPON:BAZOOKA

  21, NPC:HP UP

end


global variable, begin
  # game state
  1,  playing
  2,  score
  3,  lives left
  4,  used door

  # player control suspension variables
  5,  moving suspended
  6,  shooting suspended

  # platformer general settings
  10, friction
  11, gravity

  # platformer hero settings
  20, hero-accel
  21, hero-jump-accel
  22, hero-max-vx
  23, hero-max-vy
  24, hero-jump-speed
  25, hero-hurt-propel

  26, hero-shoot-rate
  27, hero-invincibility-time
  28, hero-max-hp

  # platformer hero state
  30, hero slice
  31, hero-x
  32, hero-y
  33, hero-vx
  34, hero-vy
  35, hero-animation
  36, hero-direction
  37, point direction  #hero direction, overridden by up and down keys
  38, hero can jump

  40, hero-invincibility-tick
  41, time since shot
  42, hero weapon
  43, weapon sprite
  44, natural target  #slice handle, eg boss weak point, for homing missiles

  # AI
  50, num guards
  51, num paths
  #52, current path slice  #for shortcut funcs in misc.hss

  # misc
  60, one use tag
  61, skip door
  62, test sl   # for debugging

  # layers
  100, target layer
  101, ally target layer
  102, bullet layer
  103, ally bullet layer
  104, gfx layer

  110, rain parent
  111, old camera x
  112, old camera y


  #31, boss-hp
  #32, boss-max-hp
  #33, boss-invincibility
  119, boss-flash-tick

  120, snake boss
  121, snake boss dying
  122, snake vx
  123, snake vy


  #150, ARRAY:BOSS PIECES

  #200, ARRAY:PATH
  #1000, ARRAYEND:PATH

end


#========================================

plotscript, new game, begin
#$31="", append number(31, sine(-34, 10000)), trace (31)

  one use tag := 1200
  skipdoor := -1

  initialise ai
  initialise layers
  initialise platformer
  set timer (main loop timer, 0, 1, @do game)

  inc hero hp (6)
  hero weapon := WEAPON:PISTOL

  map 0
  custom main loop
  game over
end


script, custom main loop, begin
  # This is for triggering cutscenes and anything else that may require 

  while (playing) do (
    if (hero x(me) >> 15 && current map == 0 && check tag(2)==off) then (intro)

    if (current map == 1) then (do snake boss)

    wait (1)
  )
end


script, HUD, begin
  variable (i, spr)

  $10="HP :"
  for (i, 1, get slice extra (hero slice, TARGETX:hp)) do (
    append ascii (10, 186)
    append ascii (10, 255)
  )
  show string at (10, 0, 0)

  if (hero weapon == WEAPON:PISTOL) then (spr := 0) else (spr := 3)

  if (weapon sprite == 0) then (
    weapon sprite := load weapon sprite (spr)
    put sprite (weapon sprite, 0, 8)
  ) else (
    if (get sprite set number(weaponsprite) <> spr) then (replace weapon sprite (weaponsprite, spr))
  )
end

plotscript, inc hero hp, howmuch, npcref=0, begin
  set slice extra (hero slice, TARGETX:hp, get slice extra (hero slice, TARGETX:hp) + howmuch)
  if (npc ref) then (destroy npc (npc ref))
  if (howmuch << 0) then (play sound (6))
end

#====================================== BEGIN CUTSCENES ==============

script, intro, begin
  moving suspended := true
  shooting suspended := true
  variable (sl)
  sl := load medium enemy sprite (0,1)
  put slice (sl, 400, 100)
  set parent (sl, lookup slice (sl: map layer 1))
  play sound (1)
  wait (20)
  show text box (1)
  wait for text box
  wait (10)
  wait (1)
  free slice (sl)
  moving suspended := false
  shooting suspended := false
  set tag (2, on)
end


#======================================== SNAKE BOSS ===============

script, pos snake, begin
  variable (i, sl)
  for (i, 0, 7) do (
    sl := read global(ARRAY:BOSSPIECES + i)
    put slice (sl, 100 + i * 20, 150)
  )
end

script, move snake, begin
  variable (i, sl,y)
  for (i, 0, 7) do (
    sl := read global(ARRAY:BOSSPIECES + i)
    put slice (sl, slicex(sl) +snakevx, slicey(sl))
  )
  y:=slicey(heroslice)--10
  snakevy :=random(-2,2)
  if (slicex(sl) << -280) then (
$31="l", trace(31)
    for (i, 0, 7) do (
      sl := read global(ARRAY:BOSSPIECES + i)
      put slice (sl, -80 -- i * 20, y)
      y += snakevy
    )
    snake vx := 25
  )
  if (slicex(sl) >> 600) then (
$31="r", trace(31)
    for (i, 0, 7) do (
      sl := read global(ARRAY:BOSSPIECES + i)
      put slice (sl, 400 + i * 20, y)
      y += snakevy
    )
    snake vx := -25
  )

end

script, initsnakeboss, begin
  variable(i, sl)

  snake boss := true

  # I want arrays
  snake vx := -25


  for (i, 7, 1, -1) do (
    if (i == 4) then (
      sl:= load small enemy sprite (5)
      sl:=new enemy (sl, 4, 0, @hurt:snakeboss)   #HP = inf
    ) else (
      sl:= load small enemy sprite (1 + random(0,1))
      sl:=new enemy (sl, 9999, 0)   #HP = inf
    )
    write global (ARRAY:BOSS PIECES + i, sl)
  )

  sl:= load small enemy sprite (6)
  set animation loop (sl, 3, 15)
  sl:=new enemy (sl, 9999, @path:dummy)   #HP = inf
  write global (ARRAY:BOSS PIECES + 0, sl)

  pos snake
end

script, do snake boss, begin
  if (snake boss) then (
    if (boss-flash-tick) then (
      boss-flash-tick -= 1
      if (boss-flash-tick == 0) then (
        variable (i, sl)
        for (i, 0, 7) do (
          sl := read global(ARRAY:BOSSPIECES + i)
          set sprite palette (firstchild(sl), -1)
        )
      )
    )
    move snake
  )

  if (snake boss dying) then (
    for (i, 0, 7) do (
      sl := read global(ARRAY:BOSSPIECES + i)
      if (slicey(sl) >> map height*20 -- 34) then (
        set slice y(sl, -1 * slicey(sl)+2*(map height*20--34))
        set vy(sl, -1 * getvy(sl))
      )
    )

    snake boss dying -= 1
    if (snake boss dying == 0) then (
      for (i, 0, 7) do (
        sl := read global(ARRAY:BOSSPIECES + i)
        free slice(sl)
      )
      snakeboss := false
      show textbox(6)
    )
  )
end

script, hurt:snakeboss, hit, damage, begin
$31="hit snake "
append number(31, damage)
trace(31)
#$31+"  seg= "
#append number(31, segment)

  if (basic check enemy death(hit,damage)) then (
    #death
    play sound (7)

    snakeboss := false
    snakeboss dying := 50

    for (i, 0, 7) do (
      sl2 := read global(ARRAY:BOSSPIECES + i)

      if (random(0,1)*random(0,1)) then(explosion1(slice x(sl2)+slicewidth(sl2)/2, slicey(sl2)+sliceheight(sl2)))
else(if (random(0,1)) then(explosion2(slice x(sl2)+slicewidth(sl2)/2, slicey(sl2)+sliceheight(sl2))))
sl3:=firstchild(sl2)

      # pull sprites out of the boss piece slices
      set parent (sl3, gfx layer)
      put slice (sl3, slice x (sl2), slice y (sl2))
      set velocity (sl3, get vx(sl2) + random(-10,10), get vy(sl2) +random(-10,10), true)
      write global (ARRAY:BOSSPIECES + i, sl3)
      free slice (sl2)
    )
  ) else (
    boss-flash-tick := 4
    variable (i, sl2, sl3)
    for (i, 0, 7) do (
      sl2 := read global (ARRAY:BOSSPIECES + i)
      set sprite palette (firstchild(sl2), 4)
    )
  )

end


#========================================  STUFF   ====================

script, hurt hero, dummy=0, damage = 1, begin
  if (hero-invincibility-time) then (exit script)

  inc hero hp (damage * -1)
  if (get slice extra (hero slice, TARGETX:hp) <= 0) then ( fade screen out, game over )
  hero-invincibility-time := 20

  #hero-x += dir X (hero-direction) * hero-hurt-propel
  hero-vx += dir X (hero-direction) * hero-hurt-propel

end

script, init map, begin
  alternpc(NPC:HP UP, NPCstat:picture, 7)
  alternpc(NPC:HP UP, NPCstat:palette, -1)
  alternpc(NPC:HP UP, NPCstat:script, @inc hero hp)
  alternpc(NPC:HP UP, NPCstat:script argument, 1)
end

#========================================   MAPS   ====================

plotscript, map 0, begin
  init map
  skip door := 0

  start rain

  enemy: walking turkey (2, 5)

  enemy: walking turkey (4, 10)

# enemy: flying turkey (10, 4)

  variable(sl)
#sl:=load walkabout sprite(23)
#set sprite frame (sl,7)
#set animation loop(sl,8,2,true)
#putslice(sl, 100,100)
#testsl:=sl
end

plotscript, map 1, begin
  init map

  stop rain

  init snake boss
end
