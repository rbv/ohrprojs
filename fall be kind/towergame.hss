include, plotscr.hsd
include, scancode.hsi
include, math.hsi
include, paths.hss
include, maps.hss
include, movement.hss
include, npcai.hss
include, misc.hss
include, beta.hsi

define constant, begin

  9, MAX RANGE ANGLE  # degrees to either side of guard that they have full range.
  70, MAX X RANGE      # maximum guard horizontal viewing ditance
  70, MAX Y RANGE      # maximum guard vertical viewing ditance

  90, TIMER LENGTH

  0, DEBUG STRINGS X
  0, DEBUG STRINGS Y

  1, SNEAK SPEED
  4, WALK SPEED
  8, RUN SPEED

  3000, HUNGER RATE  # use a constant for now. 3000 = starve in ~30min

  0, door mode:align       # walk squarely onto door tile then walk off screen
  1, door mode:directional # enter door only if player is facing the correct direction
  2, door mode:wide        # don't align hero, door is more than 1 tile wide

  100, ARRAY:GUARD
  6, SIZEOF:GUARD
  0, GUARD:NPC    # indicates unused slot if 0
#  1, GUARD:PATH
  2, GUARD:PATHPTR
#  3, GUARD:SOUNDTIMER
  4, GUARD:SUSPICION   # a measure that increases with suspicious events, eg multiple noises
  5, GUARD:VIGIL       # quantify the vigilance of a guard: higher = look around alot, inspect 

  #tiles 144, 145, 146 reserved for wipe-ins noises



  #NOTE: paths are usually passed around as a direct ptr, unlike guards which use guard number

  # Executing paths:
  # if DESTX or DESTY >> -1, then walk in direction DIR (which might be -1)
  # when that completes, DESTX and DESTY set to -1
  # then if DIR <> -1, face that direction
  # then count down WAIT until 0
  # then if SCRIPT, call it
  # else, or if the script returns -1, delete path and consider NEXTPTR

  200, ARRAY:PATH
  10, SIZEOF:PATH
  0, PATH:SEG     # next segment, path piece number for fixed paths, essentially a state variable otherwise
  1, PATH:DEST X
  2, PATH:DEST Y
  3, PATH:SPEED
  4, PATH:WAIT    # wait ticks after finished segment actions
  5, PATH:DIR
  6, PATH:SCRIPT
  7, PATH:NEXTPTR  # ptr to next path segment in queue, -1 for none, 0 for path not used
  8, PATH:PRIORITY # Used when deleting/overriding, and deciding to trigger paths, NOT for execution order
  9, PATH:ARG # Used when deleting/overriding, and deciding to trigger paths, NOT for execution order


#  300, ARRAY:ONSTEP #TRIGGERS
#  1, SIZEOF:ONSTEP

  800, SCRATCH SPACE

  34, NPCID:PEBBLE
  35, NPCID:DOOR
end

global variable, begin
  1, player npc
  2, pebble npc
  3, pebble age
  4, pebble dir
  5, pebble z
  6, pebble number

  8, door npc ignore
  9, door npc ignore level
 # 9, last player onstep

  # control
  10, want dir  # -1 if no movement requested.
  11, want throw  # launch a stone
#  12, want use    # use key pressed  (not used, dealt with in keyhandler)
  13, blocked dir  # a direction in which player movement is blocked (not used)
  14, player speed
  15, hunger value # varies from 0 up to 100(000,000) , 0-100 scale multiplied by 1000,000
  
  19, keyhandler lock # prevent multiple keyhandler instances

  20, num guards
  21, num paths

  30, sound x
  31, sound y
  32, sound vol

  # the following variables suspend functions: increment and decrement them to implement overrides
  40, no player input   # ignore all keys, for scripted movement
  41, no hero clipping  # walk through walls      (not needed for alternative hero movement scripts)
#  42, no interruptions  # don't show interruptions like messages
  43, no camera movement # no moving the camera! (eg wipe ins)       (not needed for alternative hero movement scripts)
  44, no hero step trigger  # don't check triggers (eg don't trigger destination door)
  45, no capture         # turn off player capture by guards
  46, textbox pause      # a textbox is up which the player has to advance

  # debug stuff
  50, moveable guard
  51, debug info
  52, last second
  53, frame count

  # data for worker scripts
  60, wipe:reverse
  61, wipe:ctr
  62, door:state
  63, door:npc
  64, door:id  #ID of current door

  65, destination map
  66, destination door
  67, door direction
  68, door mode
  69, walk off length

  70, run captured script  # temp script running mechanism
  71, safe point map
  72, safe point x
  73, safe point y

  75, timer over # hide, don't reset.

  # map hooks
  80, map doors table
  81, last map

end


# string use:
# 14 - 31 used for wipes
# 0 is pebble count
# 1 is timer string
# 2 is hunger counter (debug)
# 9 is fps
# 31 for free use
# 10+ is guard debug (and 20+)?



plotscript, mainloop, begin
  suspend player
  suspend overlay
  wipe:ctr := -2
  wipe:reverse := 1 # wiped in screen
  last map := -1  # the first map script will not see this!

#  player npc := create npc (0, 19, 37)
  player npc := create npc (0, read general (102), read general (103)) # use the normal starting position data
  camera follows npc (player npc)

  intro

  set pebbles (0)
  #set timer (0, TIMER LENGTH, 16, @time is up, 1)
  show string at (1, 0, 0)
  string color (1, 2)

  ##set safe point (current map, npc pixel x (player npc), npc pixel y (player npc)) # a safe default of the starting point

  hunger value := 0
  show string at (2, 0, 190)

  # run in map autorun script
  #reset map

  while (true) do, begin
    # worker scripts (called each tick, these decide for themselves whether to do anything)
    door script
    if (run captured script) then (captured worker)
    wipe screen worker

    hero hunger

    hero movement
    pebble movement
    guard movement

    sound vol := 0

    if (debug info) then (
      frame count += 1
      if (system second <> last second) then (
        clear string (9)
        append number (9, frame count)
        show string (9)
        frame count := 0
        last second := system second
      )
    )

    wait
  end

end

plotscript, keyhandler, begin
  variable (i)
  if (textbox pause) then (
    if (keyval (key:ctrl) >> 1 || keyval (key:enter) >> 1 || keyval (key:space) >> 1) then (
      #resume box advance
      #advance textbox
      textbox pause := false
    )
    exit script
  )
  if (keyhandler lock || no player input) then (exit script)
  keyhandler lock := true

  try dir (keyval (key:left), left)
  try dir (keyval (key:right), right)
  try dir (keyval (key:up), up)
  try dir (keyval (key:down), down)

#  if (keyval (key:left) >> 1 || keyval (key:left) && blocked dir <> left) then (want dir := left)
#  if (keyval (key:right) && blocked dir <> right) then (want dir := right)
#  if (keyval (key:up) && blocked dir <> up) then (want dir := up)
#  if (keyval (key:down) && blocked dir <> down) then (want dir := down)
  if (key is pressed (key:esc)) then (game over)



  ## DEBUG KEYS
  #pause
  if (key is pressed (key:p)) then (wait for key)
  #invisibility
  if (keyval (key:i) >> 1) then (no capture := no capture, xor, 1)
  #no walls
  if (keyval (key:c) >> 1) then (no hero clipping := no hero clipping, xor, 1)
  #inspect npcs
  if (keyval (key:q) >> 1) then (pick npc)
  #debug stuff?
  if (keyval (key:d) >> 1) then (debug info := debug info, xor, 1, for (i,9,14) do (hide string (i)) )
  #wipe screen
  if (key is pressed (key:w)) then (
    if (screen wipe done == 1) then (resetmapstate(mapstate:tilemap), wipe screen(true)) else (wipe screen)
  )
  #if (key is pressed (key:d)) then (debug visibility)
  #move guard
  if (key is pressed (key:g)) then (
    put npc (get guard npc (moveable guard), npc pixel x (playernpc), npc pixel y (playernpc))
    set npc direction (get guard npc (moveable guard), npc direction (player npc))
  )
  if (key is pressed (key:1)) then (
move to spot (read global(ARRAY:GUARD + SIZEOF:GUARD * moveable guard + GUARD:NPC),
              npc pixel x (playernpc), npc pixel y (playernpc), 7)
  )
  if (key is pressed (key:2)) then (
move to spot (read global(ARRAY:GUARD + SIZEOF:GUARD * moveable guard + GUARD:NPC),
              npc pixel x (playernpc), npc pixel y (playernpc), 7, npc direction(get guard npc (moveable guard)))
  )
  if (key is pressed (key:3)) then (
move to spot (read global(ARRAY:GUARD + SIZEOF:GUARD * moveable guard + GUARD:NPC),
              npc pixel x (playernpc), npc pixel y (playernpc), 7, -1, 1, 0)
  )

  if (keyval (key:space) >> 1) then (want throw := true)
#  if (keyval (key:ctrl) >> 1 || keyval (key:enter) >> 1) then (want use := true)
  if (keyval (key:ctrl) >> 1 || keyval (key:enter) >> 1) then (check npc activation)


  if (keyval (key:z) >> 1) then (
    if (walk speed <> RUN SPEED) then (
      player speed := (player speed == SNEAK SPEED) * WALK SPEED + (player speed == WALK SPEED) * SNEAK SPEED
    )
  )
  keyhandler lock := false

end

script,dummy(wait)

# project for another day
script, try dir, pressval, dir, begin
  if (pressval) then (want dir := dir)

#  if ((pressval >> 1) || (pressval == 1 && blocked dir <> dir)) then (
#    want dir := dir
#  )
end

script, intro, begin
#set music volume (0)
  set on key press script

  wait
  show text box (3)
#  wait for textbox

  suspend box advance
  wait for key
  fade screen out
  wait
  resume box advance
  advance textbox
  wait
  fade screen in

  set on key press script (@keyhandler)
end

script, reset map, begin
  want dir := -1
  blocked dir := -1
  player speed := WALK SPEED

  set on key press script (@keyhandler)

  #default npcs for lazy people
  alter npc (NPCID:DOOR, NPCstat:script, @door)
  alter npc (NPCID:DOOR, NPCstat:activation, NPCactivation:stepon)
# alter npc (NPCID:DOOR, NPCstat:script argument, 0)   #not used
  #pebble npc
  alter npc (NPCID:PEBBLE, NPCstat:picture, 5) 
  alter npc (NPCID:PEBBLE, NPCstat:palette, 0)
  #hero
  alter npc (0, NPCstat:picture, 0) 
  alter npc (0, NPCstat:palette, 1)
  alter npc (1, NPCstat:picture, 1) 
  alter npc (1, NPCstat:palette, 1)

  put hero (0, 0)

  sound vol := 0

  # delete all guards
  variable (considered, offset)

  for (offset, ARRAY:GUARD, 999, SIZEOF:GUARD) do (
    if (considered == num guards) then (break)

    if (read global (offset + GUARD:NPC)) then (
      considered += 1
      write global (offset + GUARD:NPC, 0)
    )
  )
  num guards := 0

  # delete all paths
  considered := 0
  for (offset, ARRAY:PATH, 999, SIZEOF:PATH) do (
    if (considered == num paths) then (break)

    if (read global (offset + PATH:NEXTPTR)) then (
      considered += 1
      write global (offset + PATH:NEXTPTR, 0)
    )
  )
  num paths := 0
end

# normally hero movement is controlled via player input in keyhandler
# this can be disabled with noplayerinput
# scripting hero movement can be achieved either by setting wantdir,
# or by writing a separate script to move the hero
script, hero movement, begin
  if (want dir == -1) then (exit script)

  if (move in direction (player npc, want dir, player speed, not (no hero clipping))) then (
    if (no camera movement) else (camera follows npc (player npc))
  ) else (
    blocked dir := want dir
  )
  want dir := -1
end

script, pebble movement, begin
  variable (want x, want y)

  if (pebble age << 0) then (
    # pebble hit something animation

    destroy npc (pebble npc)
    pebble npc := 0
    pebble age := 0
    exit script
  )

  if (pebble npc) then (
    variable (x, y, result, old z, new z)
    switch (pebble dir) do, begin
      case (up) do (want y -= 9)
      case (down) do (want y := 9)
      case (left) do (want x -= 9)
      case (right) do (want x := 9)
    end

    pebble age += 1

    x := npc pixel x (pebble npc)
    y := npc pixel y (pebble npc) -- pebble z

    pebble z += pebble age / 3

    result := check walls plus (x, y, 4, 20, want x, want y, pebble dir)

    if (pebble age >= 8 || not (result)) then (
      variable (hit npc)
      hit npc := create npc (NPCID:PEBBLE, 0, 0, 2)
      if (not (result)) then (
        switch (pebble dir) do, begin
          case (up) do (y := (y / 20) * 20 -- 2)
          case (down) do (y := ((y + 19) / 20) * 20 -- 2)
          case (left) do (x := (x / 20) * 20 -- 2)
          case (right) do (x := ((x + 19) / 20) * 20 -- 2)
        end
        put npc (hit npc, x, y + pebble z)
        sound x := x
        sound y := y
      ) else (
        put npc (hit npc, x + want x, y + want y + pebble z)
        sound x := x + want x
        sound y := y + want y
      )
      sound vol := 60

      destroy npc (pebble npc)
      pebble npc := hit npc
      pebble age := -1
      exit script
    )

    put npc (pebble npc, x + want x, y + want y + pebble z)
  )

  if (want throw && not (pebble npc) && pebble number) then (
    pebble dir := npc direction (player npc)
    pebble z := 8

    x := npc pixel x (player npc)
    y := npc pixel y (player npc)

    if (check walls plus (x + 8, y, 4, 20, 20 * dir X (pebble dir), 20 * dir Y (pebble dir), pebble dir)) then (
      pebble npc := create npc (NPCID:PEBBLE, 0, 0, 0)
      pebble age := 0
      switch (pebble dir) do, begin
        case (up) do (x += 7, y -= 8)
        case (down) do (x += 7, y += 12)
        case (left) do (x -= 4)
        case (right) do (x += 20)
      end
      put npc (pebble npc, x, y + pebble z)

      set pebbles (pebble number -- 1)
    ) else (
#      pebble npc := create npc (NPCID:PEBBLE, 0, 0, 2)
#      pebble age := -1
#      sound x := x + want x
#      sound y := y + want y
#      sound vol := 80
    )
#    put npc (pebble npc, x + want x, y + want y + pebble z)
  )

  want throw := 0
end

# wipe:reverse = 0 wipes screen out
# wipe:reverse = 1 wipes screen in
# wipe-in requires unmodified map state, wipe-out destroys the map state
# this scritp sets up wipescreenworker to be called each tick
script, wipe screen, reverse=0, begin
  variable (i, j, camera x, camera y, free string, tile ptr)
  camera x := camera pixel x / 20
  camera y := camera pixel y / 20
  free string := 14

  if (wipe:ctr >= 0) then (exit script)

  # setup
  if (reverse) then (
$31="wipe in x ="
append number(31, camera x)
$31+" y ="
append number(31, camera y)
trace(31)
    tile ptr := SCRATCH SPACE
    for (i, camera x, camera x + 16) do (
      for (j, camera y, camera y + 10) do (
        write global (tile ptr, read map block (i, j, 2))
        write map block (i, j, 145, 2)
        tile ptr += 1
      )
    )
    #wait
  )

  for (i, free string, 31) do (
    string style (i, string:flat)
    string color (i, 240)
    #string style (i, string:outline)
    #string color (i, 15)
    clear string (i)
    if (reverse) then (append ascii (i, 185)) else (append ascii (i, 184))
  )

  wipe:ctr := camera x
  wipe:reverse := reverse

  no camera movement += 1
  put camera (camera pixel x, camera pixel y)
end

# returns 0 while wiping, 1 for wiped out, 2 for wiped in
script, screen wipe done, begin
  if (wipe:ctr << 0) then (return (wipe:reverse + 1)) else (return (0))
end


# called each tick, call wipescreen to trigger a wipe
script, wipe screen worker, begin
  variable (i, j, camera x, camera y, last line hack, free string, fringe pixel x, tile ptr, loop)

  if (wipe:ctr >= -1) then (
    free string := 14
    for (i, free string, 31) do (
      hide string (i)
    )
    if (wipe:ctr == -1) then (wipe:ctr := -2)
  )
  if (wipe:ctr << 0) then (exit script)
  camera x := camera pixel x / 20
  camera y := camera pixel y / 20
  lastlinehack := false

  for (loop, 0, 2) do (
    i := loop + wipe:ctr
    if (i >> camera x + 16) then (wipe:ctr := -1, no camera movement -= 1, exit script)   # end wipe

    tile ptr := SCRATCH SPACE + (i -- camera x) * 11

    if (loop == 2) then (
      if (wipe:reverse) then (
        fringe pixel x := i * 20 -- camera pixel x + 12  # this is in screen pixel coordinates
      ) else (
        fringe pixel x := i * 20 -- camera pixel x
      )

      for (j, camera y, camera y + 10) do (
        variable (map tile)
        if (wipe:reverse) then (
          map tile := read global (tile ptr)
          write map block (i, j, map tile, 2)
        ) else (
          map tile := read map block (i, j, 2)
        )

        if (map tile) then (
          # hack around this by placing a string there!
          if (lastlinehack) then (
            show string at (free string, fringe pixel x, j * 20 + 4 -- camera pixel y)
            show string at (free string + 1, fringe pixel x, j * 20 + 12 -- camera pixel y)
            free string += 2
            lastlinehack := false
          ) else (
            show string at (free string, fringe pixel x, j * 20 -- camera pixel y)
            show string at (free string + 1, fringe pixel x, j * 20 + 8 -- camera pixel y)
            show string at (free string + 2, fringe pixel x, j * 20 + 16 -- camera pixel y)
            free string += 3
            lastlinehack := true
          )
        ) else (
          if (wipe:reverse) then (
            write map block (i, j, 144, 2)
          ) else (
            write map block (i, j, 146, 2)
          )
          lastlinehack := false
        )
        tile ptr += 1
      )
    ) else (
      for (j, camera y, camera y + 10) do (
        if (wipe:reverse) then (
          write map block (i, j, read global (tile ptr), 2)
        ) else (
          write map block (i, j, 145, 2)
        )
        tile ptr += 1
      )
    )
  )
  wipe:ctr += 2
end

# NOTE: this script should be set on an npc, but should NOT be called by activating that npc!
# use read npc (npc, NPCstat:script) and run script by ID instead, with the following arguments
# doorref is the reference to the npc with the script set
# npcref is the npc that stepped on it
# arg is the arg set on the npc (not used)
plotscript, door, doorref, arg, npcref, begin
  variable (door num)

  if (npcref <> player npc) then (exit script)

  do (
    for (door num, 0, npc copy count (NPCID:DOOR) -- 1) do (
      if (doorref == npc reference (NPCID:DOOR, door num)) then (door:id := door num, break (2))
    )
    exit script
  )

  destination map := -1
  run script by ID (map doors table, door:id)
  if (destination map == -1) then (
    door npc ignore := doorref
    door npc ignore level := 2
    exit script
  )

  if (npc direction (npcref) <> door direction) then (
    if (door mode == door mode:directional || door npc ignore == doorref) then (
      door npc ignore := doorref
      exit script
    )
  ) else (
  )

  if (door mode == door mode:align || door mode == door mode:directional) then (door:state := 1) else (door:state := 2)

  wipe screen
  no player input += 1
  no hero step trigger += 1
  no hero clipping += 1

  # lock current position
  put camera (camera pixel x, camera pixel y)
  no camera movement += 1

end

script, door script, begin
  if (door:state) then (
    variable (x, y)
    door:npc := npc reference (NPCID:DOOR, door:id)

    # align
    if (door:state == 1) then (
      if (move to spot (player npc, npc pixel x (door:npc), npc pixel y (door:npc), player speed, ((door direction + 1) mod, 4)) == 2)
      then (set npc direction (player npc, door direction), door:state := 2)
    )

    # walk off
    if (door:state == 2) then (
      if (not (screen wipe done)) then (
        if (walk off length) then (
          #want dir := door direction

          x := npc pixel x (door:npc)
          y := npc pixel y (door:npc)
          switch (door direction) do, begin
            case (up) do (y -= 20 * walk off length)
            case (down) do (y += 20 * walk off length)
            case (left) do (x -= 20 * walk off length)
            case (right) do (x += 20 * walk off length)
          end
          move to spot (player npc, x, y, player speed, ((door direction + 2) mod, 4), 0)
        )
      ) else (door:state := 3) 
    )

    # load map
    if (door:state == 3) then (
      tweak palette (-63, -63, -63)
      update palette

      door:id := destination door
      last map := current map
 
      teleport to map (destination map)
      run script by ID (map doors table, door:id)

      door:npc := npc reference (NPCID:DOOR, door:id)
      x := npc pixel x (door:npc)
      y := npc pixel y (door:npc)
      put camera (x -- 150, y -- 90)

      #clearstring(31)
      #append number (31,map doors table)
      #append ascii (31, 32)
      #append number (31,door:id)
      #append ascii (31, 32)
      #append number (31,destination door)
      #append ascii (31, 32)
      #append number (31,destination map)
      #append ascii (31, 32)
      #append number (31,current map)
      #append ascii (31, 32)
      #append number (31,door direction)
      #append ascii (31, 32)
      #append number (31,walk off length)
      #trace(31)

    
      player npc := create npc (0, 0, 0, (door direction + 2) mod, 4)
  
      switch (door direction) do, begin
        case (up) do (y -= 20 * walk off length)
        case (down) do (y += 20 * walk off length)
        case (left) do (x -= 20 * walk off length)
        case (right) do (x += 20 * walk off length)
      end
      put npc (player npc, x, y)
  
      wipe screen (1)
      wait
      reset palette
      update palette

      door:state := 4
    )
  
    # walk in
    if (door:state == 4) then (
      if (not (screen wipe done)) then (
        if (walk off length) then (
          #want dir := (door direction + 2) mod, 4
          move to spot (player npc, npc pixel x (door:npc), npc pixel y (door:npc), player speed, ((door direction + 2) mod, 4), 0)
        )
      ) else (door:state := 5)
    )

    # clean up
    if (door:state == 5) then (
      no player input -= 1
      no hero step trigger -= 1
      no hero clipping -= 1
      no camera movement -= 1
      door:state := 0
      door npc ignore := door:npc
    )
  )
end

# the player pressed the use key
#NOTE: npc ref and argument are passed in opposite to normal order, for convenience.
script, check npc activation, begin
  variable (x, y, i, ref, trigger script)
  x := npc pixel x (player npc) + 10 + 20 * dir X (npc direction (player npc))
  y := npc pixel y (player npc) + 10 + 20 * dir Y (npc direction (player npc))

  for (i, 0, npc at pixel (x, y, get count)) do (
    ref := npc at pixel (x, y, i)
    activate npc (player npc, ref, NPCactivation:use)
  )
end

script, activate npc, actor npc, npc, activation type, begin
  # if some other npc bumps into the player, reverse roles
  if (activation type == NPCactivation:touch && npc == player npc) then (npc := actor npc)
  else (
    # for now, restrict interaction just to the hero
    if (actor npc <> player npc) then (exit script)
  )
  
  if (read npc (npc, NPCstat:script)) then (
    run script by ID (read npc (npc, NPCstat:script), read npc (npc, NPCstat:script argument), npc)
  )
  if (read npc (npc, NPCstat:display text)) then (tb (read npc (npc, NPCstat:display text)))
  if (read npc (npc, NPCstat:give item)) then (get item (read npc (npc, NPCstat:give item) -- 1))
  if (read npc (npc, 11)) then (set tag (read npc (npc, 11) + 1000, true))
end

plotscript, pick up pebbles, arg, npc ref, begin
  if (pebble number << 15) then (
    set pebbles (15)
    tb (2)
  )
end

script, captured worker, begin
  if (screen wipe done == 2) then (
    if (run captured script == 1) then (
      #begin
      wipe screen
      no player input += 1
      run captured script := 2
    ) else (
      run captured script := false
      no player input -= 1
    )
  )
  if (screen wipe done == 1) then (
    #end
    tweak palette (-63, -63, -63)
    update palette

    resume box advance
    advance textbox

    if (not (timer over)) then (
      show string at (1, 0, 0)
    #  clear string (1)  # silliness on part of timers
      #set timer (0, TIMER LENGTH, 16, @time is up, 1)
    )

    teleport to map (safe point map)
    player npc := create npc (0, 0, 0, 2)
    put npc (player npc, safe point x, safe point y)
    put camera (npc pixel x (player npc) -- 150, npc pixel y (player npc) -- 90)

    #set pebbles (0)

    wipe screen (1)
    wait
    reset palette
    update palette
  )
end

script, time is up, begin
  tb (random (5, 8))
  timer over := true
  hide string (1)
end

script, hero hunger, begin
  hunger value += HUNGER RATE
  if (hunger value >> 100 * 1000000) then (hunger value := 100 * 1000000)

  $2="hunger:"
  append number (2, hunger value / 1000000)
  $2+" "
  variable (time)
  time := 10* (100 * 1000000 -- hunger value) / (183 * HUNGER RATE)
  append number (2, time / 60)
  $2+":"
  if (time, mod, 60 << 10) then ($2+"0")
  append number (2, time, mod, 60)
  $2+" to go"
end
