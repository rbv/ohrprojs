
scale=60%
# 80 / $scale
wid=133

convert badboymockup.png -filter catrom -scale $scale  badboy1_half2.png

infile=back1.png
outfiles=bitshalf/back


convert $infile -scale $scale  ${outfiles}_full.bmp

convert $infile -scale $scale -crop 320x200+0+0 -repage 320x200 ${outfiles}0.bmp
convert $infile -scale $scale -crop 320x200+320+0 ${outfiles}1.bmp
convert $infile -scale $scale -crop 320x200+0+200 ${outfiles}2.bmp
convert $infile -scale $scale -crop 320x200+320+200 ${outfiles}3.bmp
convert $infile -scale $scale -crop 320x200+640+0 -extent 320x200 ${outfiles}4.bmp
convert $infile -scale $scale -crop 320x200+640+200  -extent 320x200 ${outfiles}5.bmp

#$outdir/$texname_$(printf "%03d" $height).bmp

mkdir bitshalf

for infile in bitsfull/*; do 
    outfile=bitshalf/$(basename $infile .png)
    echo $infile $outfile

#args="-colorspace RGB -filter Cosine -resize $scale -colorspace sRGB -map pal.bmp"
args="-filter LanczosRadius -resize $scale"
args="-resize $scale"


    W=$(identify $infile |sed -e "s/.* \([0-9]\+\)x[0-9]\+ .*/\1/")
    H=$(identify $infile |sed -e "s/.* [0-9]\+x\([0-9]\+\) .*/\1/")

     [ $W -gt $wid -a $H -gt $wid ] && 
    convert $infile $args ${outfile}full.bmp
    convert $infile $args -crop 80x80+0+0 ${outfile}T.bmp
     [ $W -gt $wid ] && 
    convert $infile $args -crop 80x80+80+0 ${outfile}R.bmp
     [ $H -gt $wid ] && 
    convert $infile $args -crop 80x80+0+80 ${outfile}B.bmp

done
