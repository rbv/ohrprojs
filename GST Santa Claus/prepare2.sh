
infile=border3.png
outfiles=bitshalf/border

convert $infile -crop 320x200+0+0 -repage 320x200 ${outfiles}0.bmp
convert $infile -crop 320x200+320+0 ${outfiles}1.bmp
convert $infile -crop 320x200+640+0 -extent 320x200 ${outfiles}2.bmp

convert $infile -crop 320x200+0+200 -repage 320x200 ${outfiles}3.bmp
convert $infile -crop 320x200+320+200 ${outfiles}4.bmp
convert $infile -crop 320x200+640+200 -extent 320x200 ${outfiles}5.bmp

convert $infile -crop 320x200+0+400 -extent 320x200  ${outfiles}6.bmp
convert $infile -crop 320x200+320+400 -extent 320x200  ${outfiles}7.bmp
convert $infile -crop 320x200+640+400 -extent 320x200 ${outfiles}8.bmp


convert $infile -crop 320x200+0+600 -extent 320x200  ${outfiles}9.bmp
convert $infile -crop 320x200+320+600 -extent 320x200  ${outfiles}10.bmp
convert $infile -crop 320x200+640+600 -extent 320x200 ${outfiles}11.bmp
