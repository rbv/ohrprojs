BADBOY
(C) Copyright 2015 Hawk, TMC & Giz

A Game & Watch-style minigame.
The Devil has been turning all the boys and girls naughty. Santa has
to stop a boy from breaking vases while dodging imps sent to stop
him. Can he succeed in redeeming Dennis the Menace?

This is the prize for the Christmas Wish Contest 2015 [1] prizewinner,
sheamkennedy, who provided [2] the initial concept for the
game. Although multiple minigames were meant to be included and linked
together, as sheamkennedy specified, we only finished one (you can
amuse yourself by reading the full design doc in scripts/).
See you next year! Maybe.

[1]: http://www.slimesalad.com/forum/viewtopic.php?t=6766
[2]: http://www.slimesalad.com/forum/viewtopic.php?p=120060#120060

Controls:

Left/A: Move to left slot; flip if left
Down/S: Move to center slot; flip if center
Right/D: Move to right slot; flip if right
Pause: pause the game
Space: flip Santa left/right
ESC: quit

