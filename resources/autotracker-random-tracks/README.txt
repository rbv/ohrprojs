Randomly generated music.
See https://github.com/bob-the-hamster/autoautotracker for more information.
This music is all public domain. You can do anything you want with it.